// Bufmetrics provides a buffer for metrics.Interface.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package bufmetrics

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"io"
	"sync"
	"time"
)

// Buffer is a metrics.Interface backed by a buffer.
type Buffer interface {
	io.Closer
	metrics.Interface
	log.Logable
}

// bufMetrics is an implementation of a Buffer.
type bufMetrics struct {
	log.BasicLogable
	c        chan<- []*metrics.Point // Buffer submission channel
	stop     func() error            // The stop function
	m        sync.Mutex              // Controls access to the following
	buf      []*metrics.Point        // The buffer
	isClosed bool                    // Are we closed?
	closeErr error                   // The close error (if any)
}

// queueSize is the number of buffers that can be queued for submission.
const queueSize = 2

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// submitSlice submits the slice S of metrics data to m. Recovers from any panics.
func submitSlice(ctx context.Context, m metrics.Interface, S []*metrics.Point) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in metrics.Interface: %v", e)
		}
	}()
	err = metrics.SubmitSlice(ctx, m, S)
	return
}

// submitWorker listens for slices from the submitC channel, submitting them to m with flushTimeout. Any errors will be recorded in lg. The worker will exit when the submitC channel closes. The doneC channel will be closed on exit. Intended to be run in its own go-routine.
func submitWorker(m metrics.Interface, flushTimeout time.Duration, submitC <-chan []*metrics.Point, doneC chan<- struct{}, lg log.Interface) {
	defer close(doneC)
	for S := range submitC {
		if len(S) != 0 {
			ctx, cancel := context.WithTimeout(context.Background(), flushTimeout)
			if err := submitSlice(ctx, m, S); err != nil {
				lg.Printf("Error submitting buffer: %v", err)
			}
			cancel()
		}
	}
}

// tickerWorker calls the submit function every flushInterval. The worker will exit when exitC fires. Intended to be run in its own go-routine.
func tickerWorker(submit func(), flushInterval time.Duration, exitC <-chan struct{}) {
	t := time.NewTicker(flushInterval)
	defer t.Stop()
	for {
		select {
		case <-t.C:
			submit()
		case <-exitC:
			return
		}
	}
}

// createStopFunc returns a function that stops the background workers and closes the given metrics.
func createStopFunc(m metrics.Interface, submitC chan<- []*metrics.Point, exitC chan<- struct{}, doneC <-chan struct{}) func() error {
	return func() (err error) {
		// Ask the background workers to exit
		close(exitC)
		close(submitC)
		// Wait for the submit worker to exit. Note that this may take time
		// (queueSize + 1) * flushTimeout
		<-doneC
		// Close the metrics endpoint (if relevant)
		if cm, ok := m.(io.Closer); ok {
			err = cm.Close()
		}
		return
	}
}

/////////////////////////////////////////////////////////////////////////
// bufMetrics functions
/////////////////////////////////////////////////////////////////////////

// New returns a new buffer wrapping m, with given buffer size, flush interval (how frequently the buffer is automatically submitted to m), and flush timeout (how long before a submission attempt to m is determined to have timed out). It is important that the user call Close on the returned buffer, otherwise unflushed metric data will be lost.
func New(m metrics.Interface, size int, flushInterval time.Duration, flushTimeout time.Duration) (Buffer, error) {
	// Sanity check
	if size <= 0 {
		return nil, fmt.Errorf("buffer size (%d) must be a positive integer", size)
	} else if flushInterval <= 0 {
		return nil, fmt.Errorf("flush interval (%s) must be a positive duration", flushInterval)
	} else if flushTimeout <= 0 {
		return nil, fmt.Errorf("flush timeout (%s) must be a positive duration", flushTimeout)
	}
	// If no metrics.Interface was given, discard all points
	if m == nil {
		m = metrics.Discard
	}
	// Create the communication channels
	exitC, doneC := make(chan struct{}), make(chan struct{})
	submitC := make(chan []*metrics.Point, queueSize)
	// Create the new buffer
	b := &bufMetrics{
		c:    submitC,
		stop: createStopFunc(m, submitC, exitC, doneC),
		buf:  make([]*metrics.Point, 0, size),
	}
	// Start the ticker worker
	go tickerWorker(func() {
		b.m.Lock()
		defer b.m.Unlock()
		b.flushNoLock()
	}, flushInterval, exitC)
	// Start the submit worker
	go submitWorker(m, flushTimeout, submitC, doneC, b.Log())
	// Return the buffer
	return b, nil
}

// flushNoLock flushes the buffer (if necessary). Assumes that the caller owns a lock on the mutex.
func (b *bufMetrics) flushNoLock() {
	if len(b.buf) != 0 {
		select {
		case b.c <- b.buf:
		default:
			b.Log().Printf("Discarding buffer (size %d)", len(b.buf))
		}
		b.buf = make([]*metrics.Point, 0, cap(b.buf))
	}
}

// Close closes ensures that the buffer is flushed, and prevent further points being submitted. If the wrapped metrics.Interface satisfies the io.Closer interface, then the wrapped metrics.Interface's Close method will be called.
func (b *bufMetrics) Close() error {
	// Acquire a lock
	b.m.Lock()
	defer b.m.Unlock()
	// Is there anything to do?
	if !b.isClosed {
		b.flushNoLock()
		b.isClosed = true
		b.closeErr = b.stop()
	}
	// Return any errors
	return b.closeErr
}

// Submit submits the given point.
func (b *bufMetrics) Submit(_ context.Context, p *metrics.Point) error {
	// Is there anything to do?
	if p == nil {
		return nil
	}
	// Acquire a lock
	b.m.Lock()
	defer b.m.Unlock()
	// Are we closed?
	if b.isClosed {
		return errors.New("attempting to submit to a closed buffer")
	}
	// If necessary flush the buffer
	if len(b.buf) == cap(b.buf) {
		b.flushNoLock()
	}
	// Append the point to the buffer
	b.buf = append(b.buf, p)
	return nil
}

// SubmitSlice submits the given slice of points.
func (b *bufMetrics) SubmitSlice(_ context.Context, ps []*metrics.Point) error {
	// Is there anything to do?
	if len(ps) == 0 {
		return nil
	}
	// Acquire a lock
	b.m.Lock()
	defer b.m.Unlock()
	// Are we closed
	if b.isClosed {
		return errors.New("attempting to submit to a closed buffer")
	}
	// Start working through the points
	for _, p := range ps {
		if p != nil {
			// If necessary flush the buffer
			if len(b.buf) == cap(b.buf) {
				b.flushNoLock()
			}
			// Append the point to the buffer
			b.buf = append(b.buf, p)
		}
	}
	return nil
}

// SubmitPoints submits the points in the given iterator.
func (b *bufMetrics) SubmitPoints(_ context.Context, itr metrics.PointIterator) error {
	// Acquire a lock
	b.m.Lock()
	defer b.m.Unlock()
	// Are we closed
	if b.isClosed {
		return errors.New("attempting to submit to a closed buffer")
	}
	// Start working through the points
	for itr.Next() {
		if p := itr.Value(); p != nil {
			// If necessary flush the buffer
			if len(b.buf) == cap(b.buf) {
				b.flushNoLock()
			}
			// Append the point to the buffer
			b.buf = append(b.buf, p)
		}
	}
	return itr.Err()
}
