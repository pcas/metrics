/*
Package metricsreporter provides a reporter for metrics data collected by package "github.com/rcrowley/go-metrics".

This is a reporter for package "github.com/rcrowley/go-metrics" which will submit the metrics to a metrics.Interface. See:
   https://godoc.org/github.com/rcrowley/go-metrics
   https://godoc.org/bitbucket.org/pcas/metrics

Example usage:
	import (
		"bitbucket.org/pcas/metrics/metricsreporter"
		"bitbucket.org/pcastools/log"
		"github.com/rcrowley/go-metrics"
	)

	defer metricsreporter.Register(
		m,                       // The destination metrics.Interface
		metrics.DefaultRegistry, // The source go-metrics.Registry
		10 * time.Second,        // The reporting interval
		log.Stderr,              // Log any errors to stderr
	).Stop()

*/
package metricsreporter

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/log"
	"context"
	gometrics "github.com/rcrowley/go-metrics"
	"time"
)

// channelIterator wraps a channel and implements metrics.PointIterator.
type channelIterator struct {
	c <-chan *metrics.Point // The underlying channel
	p *metrics.Point        // The current value
}

// Reporter links a go-metrics.Registry to a metrics.Interface.
type Reporter struct {
	cancel context.CancelFunc // The cancel function for the worker context
}

// DefaultInterval is the default reporting interval.
const DefaultInterval = 10 * time.Second

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// metricsToChannel converts the metrics from r to points, and feeds them down the channel c. The channel will be closed when finished. Any unsupported metric types or errors will be logged to l, and the metric will be skipped.
func metricsToChannel(r gometrics.Registry, c chan<- *metrics.Point, l log.Interface) {
	// Iterator over the metrics in the registry
	r.Each(func(name string, metric interface{}) {
		var fields metrics.Fields
		var tags metrics.Tags
		// Create the fields for this metric
		switch metric := metric.(type) {
		case gometrics.Counter:
			ms := metric.Snapshot()
			tags = metrics.Tags{
				"type": "counter",
			}
			fields = metrics.Fields{
				"value": ms.Count(),
			}
		case gometrics.Gauge:
			ms := metric.Snapshot()
			tags = metrics.Tags{
				"type": "gauge",
			}
			fields = metrics.Fields{
				"value": ms.Value(),
			}
		case gometrics.GaugeFloat64:
			ms := metric.Snapshot()
			tags = metrics.Tags{
				"type": "gauge",
			}
			fields = metrics.Fields{
				"value": ms.Value(),
			}
		case gometrics.Histogram:
			ms := metric.Snapshot()
			ps := ms.Percentiles([]float64{0.5, 0.75, 0.95, 0.99, 0.999, 0.9999})
			tags = metrics.Tags{
				"type": "histogram",
			}
			fields = metrics.Fields{
				"count":    ms.Count(),
				"max":      ms.Max(),
				"mean":     ms.Mean(),
				"min":      ms.Min(),
				"stddev":   ms.StdDev(),
				"variance": ms.Variance(),
				"p50":      ps[0],
				"p75":      ps[1],
				"p95":      ps[2],
				"p99":      ps[3],
				"p999":     ps[4],
				"p9999":    ps[5],
			}
		case gometrics.Meter:
			ms := metric.Snapshot()
			tags = metrics.Tags{
				"type": "meter",
			}
			fields = metrics.Fields{
				"count": ms.Count(),
				"m1":    ms.Rate1(),
				"m5":    ms.Rate5(),
				"m15":   ms.Rate15(),
				"mean":  ms.RateMean(),
			}
		case gometrics.Timer:
			ms := metric.Snapshot()
			ps := ms.Percentiles([]float64{0.5, 0.75, 0.95, 0.99, 0.999, 0.9999})
			tags = metrics.Tags{
				"type": "timer",
			}
			fields = metrics.Fields{
				"count":    ms.Count(),
				"max":      ms.Max(),
				"mean":     ms.Mean(),
				"min":      ms.Min(),
				"stddev":   ms.StdDev(),
				"variance": ms.Variance(),
				"p50":      ps[0],
				"p75":      ps[1],
				"p95":      ps[2],
				"p99":      ps[3],
				"p999":     ps[4],
				"p9999":    ps[5],
				"m1":       ms.Rate1(),
				"m5":       ms.Rate5(),
				"m15":      ms.Rate15(),
				"meanrate": ms.RateMean(),
			}
		default:
			// Unsupported metric type
			l.Printf("Skipping unsupported metric \"%s\" of type %T", name, metric)
		}
		// Create the point and pass it down the channel
		if len(fields) != 0 {
			p, err := metrics.NewPoint(name, fields, tags)
			if err != nil {
				l.Printf("Error converting metric \"%s\" to point: %s", name, err)
			} else {
				c <- p
			}
		}
	})
	// Close the channel
	close(c)
}

// submitMetrics submits the metrics from r to m.
func submitMetrics(ctx context.Context, m metrics.Interface, r gometrics.Registry, l log.Interface) error {
	// Create the iterator
	c := make(chan *metrics.Point, 100)
	go metricsToChannel(r, c, l)
	itr := &channelIterator{c: c}
	// Submit the contents of the iterator to m
	err := metrics.SubmitPoints(ctx, m, itr)
	// Ensure that the iterator is drained before returning
	for itr.Next() {
	}
	return err
}

// worker sets the background worker running feeding metrics from r to m every interval. Intended to be run in its own go-routine.
func worker(ctx context.Context, m metrics.Interface, r gometrics.Registry, interval time.Duration, l log.Interface) {
	t := time.NewTicker(interval)
	defer t.Stop()
	for {
		select {
		case <-ctx.Done():
			// We've been asked to exit
			return
		case <-t.C:
			// Submit the metrics
			err := submitMetrics(ctx, m, r, l)
			if err != nil {
				l.Printf("Error submitting metrics: %s", err)
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////
// channelIterator functions
/////////////////////////////////////////////////////////////////////////

// Close is a no-op, and always return nil.
func (*channelIterator) Close() error {
	return nil
}

// Err is a no-op, and always returns nil.
func (*channelIterator) Err() error {
	return nil
}

// Next advances the iterator. Returns true if there are more points in the iterator; false otherwise. Next must be called before the first call to Value.
func (itr *channelIterator) Next() (ok bool) {
	itr.p, ok = <-itr.c
	return
}

// Value returns the current point in the iterator. Attempting to call Value after the iterator is closed, or without a previous successful call to Next, may panic.
func (itr *channelIterator) Value() *metrics.Point {
	return itr.p
}

/////////////////////////////////////////////////////////////////////////
// Reporter functions
/////////////////////////////////////////////////////////////////////////

// Register starts submitting metrics from the registry to the metrics.Interface, reporting with given interval. If the interval is <= 0 then DefaultInterval will be used. Any errors submitting metrics will be logged to l.
func Register(m metrics.Interface, r gometrics.Registry, interval time.Duration, l log.Interface) *Reporter {
	// Sanity check
	if m == nil {
		m = metrics.Discard
	}
	if l == nil {
		l = log.Discard
	}
	if interval <= 0 {
		interval = DefaultInterval
	}
	// Create a new context for the worker, and start the worker
	ctx, cancel := context.WithCancel(context.Background())
	go worker(ctx, m, r, interval, l)
	// Return the reporter
	return &Reporter{
		cancel: cancel,
	}
}

// Stop stops the reporter submitting metrics from the registry.
func (rep *Reporter) Stop() {
	if rep != nil && rep.cancel != nil {
		rep.cancel()
	}
}
