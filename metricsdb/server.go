// Server handles connections from a metricsdb client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package metricsdb

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/metricsdb/internal/metricsdbrpc"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpclog"
	"bitbucket.org/pcastools/log"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/protobuf/types/known/emptypb"
	"io"

	// Make our compressors available
	_ "bitbucket.org/pcastools/grpcutil/grpcs2"
	_ "bitbucket.org/pcastools/grpcutil/grpcsnappy"
	_ "google.golang.org/grpc/encoding/gzip"
)

// The default ports that metricsdb listens on.
const (
	DefaultTCPPort = 12355
	DefaultWSPort  = 80
)

// streamIterator wraps a stream of metricsdbrpc.Points as ametrics.PointIterator.
type streamIterator struct {
	stream metricsdbrpc.Metricsdb_SubmitServer // The underlying stream
	err    error                               // Any error during iteration
	p      *metrics.Point                      // The next point
}

// metricsServer implements the tasks on the gRPC server.
type metricsServer struct {
	metricsdbrpc.UnimplementedMetricsdbServer
	log.BasicLogable
	m metrics.Interface // The destination for the metrics points
}

// Server handles client communication.
type Server struct {
	log.SetLoggerer
	grpcutil.Server
}

/////////////////////////////////////////////////////////////////////////
// streamIterator functions
/////////////////////////////////////////////////////////////////////////

// Close closes the iterator.
func (s *streamIterator) Close() error {
	if s.err == nil {
		s.err = io.EOF
	}
	return s.Err()
}

// Err returns the last error encountered during iteration, if any.
func (s *streamIterator) Err() error {
	if s.err == io.EOF {
		return nil
	}
	return s.err
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next must be called before the first call to Value.
func (s *streamIterator) Next() bool {
	if s.err != nil {
		return false
	} else if pt, err := s.stream.Recv(); err != nil {
		s.err = err
		return false
	} else if p, err := metricsdbrpc.FromPoint(pt); err != nil {
		s.err = err
		return false
	} else {
		s.p = p
	}
	return true
}

// Value returns the current point in the iterator. Attempting to call Value after the iterator is closed, or without a previous successful call to Next, may panic.
func (s *streamIterator) Value() *metrics.Point {
	if s.err != nil {
		if s.err == io.EOF {
			panic("Iteration closed")
		}
		panic(s.Err())
	} else if s.p == nil {
		panic("Iteration not started")
	}
	return s.p
}

/////////////////////////////////////////////////////////////////////////
// metricsServer functions
/////////////////////////////////////////////////////////////////////////

// Submit performs the Submit task.
func (s *metricsServer) Submit(stream metricsdbrpc.Metricsdb_SubmitServer) (err error) {
	// Defer closing the stream
	defer func() {
		if closeErr := stream.SendAndClose(&emptypb.Empty{}); err == nil {
			err = closeErr
		}
	}()
	// Wrap up the stream as an iterator
	itr := &streamIterator{stream: stream}
	defer itr.Close()
	// Submit the points
	err = metrics.SubmitPoints(stream.Context(), s.m, itr)
	return
}

/////////////////////////////////////////////////////////////////////////
// Server functions
/////////////////////////////////////////////////////////////////////////

// NewServer returns a new metricsdb server.
func NewServer(options ...Option) (*Server, error) {
	// Parse the options
	opts, err := parseOptions(options...)
	if err != nil {
		return nil, err
	}
	// Create the new server instance
	m := &metricsServer{m: opts.Metrics}
	// Create the gRPC options
	serverOptions := []grpc.ServerOption{
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpclog.UnaryServerInterceptor(m.Log()),
		)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpclog.StreamServerInterceptor(m.Log()),
		)),
	}
	// Add the SSL credentials to the gRPC options
	if len(opts.SSLKey) != 0 {
		creds, err := grpcutil.NewServerTLS(opts.SSLCert, opts.SSLKey)
		if err != nil {
			return nil, err
		}
		serverOptions = append(serverOptions, grpc.Creds(creds))
	}
	// Create the gRPC server and register our implementation
	s := grpc.NewServer(serverOptions...)
	metricsdbrpc.RegisterMetricsdbServer(s, m)
	// Bind the health data
	grpc_health_v1.RegisterHealthServer(s, health.NewServer())
	// Wrap and return the server
	return &Server{
		SetLoggerer: m,
		Server:      s,
	}, nil
}
