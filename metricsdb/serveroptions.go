// Serveroptions provides configuration options for a server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package metricsdb

import (
	"bitbucket.org/pcas/metrics"
	"errors"
)

// Option sets options on a server.
type Option interface {
	apply(*serverOptions) error
}

// funcOption wraps a function that modifies Options into an implementation of the Option interface.
type funcOption struct {
	f func(*serverOptions) error
}

// apply calls the wrapped function f on the given Options.
func (h *funcOption) apply(do *serverOptions) error {
	return h.f(do)
}

// newFuncOption returns a funcOption wrapping f.
func newFuncOption(f func(*serverOptions) error) *funcOption {
	return &funcOption{
		f: f,
	}
}

// serverOptions are the options on a server.
type serverOptions struct {
	Metrics metrics.Interface // The destination metrics
	SSLCert []byte            // The server's SSL (public) certificate
	SSLKey  []byte            // The server's SSL (private) key
}

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// parseOptions parses the given optional functions.
func parseOptions(options ...Option) (*serverOptions, error) {
	// Create the default options
	opts := &serverOptions{
		Metrics: metrics.Discard,
	}
	// Set the options
	for _, h := range options {
		if err := h.apply(opts); err != nil {
			return nil, err
		}
	}
	return opts, nil
}

// Metrics is the metrics endpoint to which metrics data will be forwarded.
func Metrics(m metrics.Interface) Option {
	return newFuncOption(func(opts *serverOptions) error {
		if m == nil {
			m = metrics.Discard
		}
		opts.Metrics = m
		return nil
	})
}

// SSLCertAndKey adds the given SSL public certificate and private key to the server.
func SSLCertAndKey(crt []byte, key []byte) Option {
	// Copy the certificate and key
	crtCopy := make([]byte, len(crt))
	copy(crtCopy, crt)
	keyCopy := make([]byte, len(key))
	copy(keyCopy, key)
	// Return the option
	return newFuncOption(func(opts *serverOptions) error {
		if len(crtCopy) == 0 {
			return errors.New("missing SSL certificate")
		} else if len(keyCopy) == 0 {
			return errors.New("missing SSL private key")
		}
		opts.SSLCert = crtCopy
		opts.SSLKey = keyCopy
		return nil
	})
}
