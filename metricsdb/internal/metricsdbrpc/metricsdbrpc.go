//go:generate protoc -I=../proto/ --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative --go_out=. --go-grpc_out=. metricsdb.proto

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package metricsdbrpc

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/convert"
	"fmt"
	"google.golang.org/protobuf/types/known/durationpb"
	"google.golang.org/protobuf/types/known/timestamppb"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// getValue returns the value set on v.
func getValue(v *Value) (val interface{}, err error) {
	switch x := v.GetValue().(type) {
	case *Value_Int32Value:
		val = x.Int32Value
	case *Value_Int64Value:
		val = x.Int64Value
	case *Value_Uint32Value:
		val = x.Uint32Value
	case *Value_Uint64Value:
		val = x.Uint64Value
	case *Value_BoolValue:
		val = x.BoolValue
	case *Value_DoubleValue:
		val = x.DoubleValue
	case *Value_StringValue:
		val = x.StringValue
	case *Value_TimestampValue:
		val = x.TimestampValue.AsTime()
	case *Value_DurationValue:
		val = x.DurationValue.AsDuration()
	default:
		err = fmt.Errorf("unsupported value type: %T", v.GetValue())
	}
	return
}

// convertType attempts to convert the object x to the type represented by t.
func convertType(x interface{}, t Value_Type) (val interface{}, err error) {
	// Perform the conversion
	switch t {
	case Value_INT:
		val, err = convert.ToInt(x)
	case Value_INT8:
		val, err = convert.ToInt8(x)
	case Value_INT16:
		val, err = convert.ToInt16(x)
	case Value_INT32:
		val, err = convert.ToInt32(x)
	case Value_INT64:
		val, err = convert.ToInt64(x)
	case Value_UINT:
		val, err = convert.ToUint(x)
	case Value_UINT8:
		val, err = convert.ToUint8(x)
	case Value_UINT16:
		val, err = convert.ToUint16(x)
	case Value_UINT32:
		val, err = convert.ToUint32(x)
	case Value_UINT64:
		val, err = convert.ToUint64(x)
	case Value_BOOL:
		val, err = convert.ToBool(x)
	case Value_FLOAT64:
		val, err = convert.ToFloat64(x)
	case Value_STRING:
		val, err = convert.ToString(x)
	case Value_TIMESTAMP:
		if s, ok := x.(time.Time); ok {
			val = s
		} else {
			// We don't make any effort to convert x to a timestamp
			err = fmt.Errorf("value type is not a timestamp: %s", t)
		}
	case Value_DURATION:
		if d, ok := x.(time.Duration); ok {
			val = d
		} else {
			// We don't make any effort to convert x to a duration
			err = fmt.Errorf("value type is not a duration: %s", t)
		}
	default:
		err = fmt.Errorf("unknown value type: %s", t)
	}
	return
}

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// FromPoint converts a *Point to a *metrics.Point.
func FromPoint(p *Point) (*metrics.Point, error) {
	// Recover the fields
	fields := p.GetFields()
	convFields := make(metrics.Fields, len(fields))
	for k, v := range fields {
		// Recover the value
		x, err := getValue(v)
		if err != nil {
			return nil, fmt.Errorf("unable to recover value for field %s: %w", k, err)
		}
		// Attempt to convert the value to the appropriate type
		val, err := convertType(x, v.GetType())
		if err != nil {
			return nil, fmt.Errorf("unable to recover value for field %s: %w", k, err)
		}
		convFields[k] = val
	}
	// Create the point
	return metrics.RawPoint(p.GetName(), convFields, metrics.Tags(p.GetTags()).Copy(), p.GetTimestamp().AsTime())
}

// ToPoint converts a *metrics.Point to a *Point.
func ToPoint(p *metrics.Point) (*Point, error) {
	// Convert the fields
	fields := p.Fields()
	convFields := make(map[string]*Value, len(fields))
	// Add the values to the map
	for k, v := range fields {
		// How we proceed depends on the type
		var val *Value
		switch x := v.(type) {
		case int:
			val = &Value{
				Type:  Value_INT,
				Value: &Value_Int64Value{Int64Value: int64(x)},
			}
		case int8:
			val = &Value{
				Type:  Value_INT8,
				Value: &Value_Int32Value{Int32Value: int32(x)},
			}
		case int16:
			val = &Value{
				Type:  Value_INT16,
				Value: &Value_Int32Value{Int32Value: int32(x)},
			}
		case int32:
			val = &Value{
				Type:  Value_INT32,
				Value: &Value_Int32Value{Int32Value: x},
			}
		case int64:
			val = &Value{
				Type:  Value_INT64,
				Value: &Value_Int64Value{Int64Value: x},
			}
		case uint:
			val = &Value{
				Type:  Value_UINT,
				Value: &Value_Uint64Value{Uint64Value: uint64(x)},
			}
		case uint8:
			val = &Value{
				Type:  Value_UINT8,
				Value: &Value_Uint32Value{Uint32Value: uint32(x)},
			}
		case uint16:
			val = &Value{
				Type:  Value_UINT16,
				Value: &Value_Uint32Value{Uint32Value: uint32(x)},
			}
		case uint32:
			val = &Value{
				Type:  Value_UINT32,
				Value: &Value_Uint32Value{Uint32Value: x},
			}
		case uint64:
			val = &Value{
				Type:  Value_UINT64,
				Value: &Value_Uint64Value{Uint64Value: x},
			}
		case bool:
			val = &Value{
				Type:  Value_BOOL,
				Value: &Value_BoolValue{BoolValue: x},
			}
		case float64:
			val = &Value{
				Type:  Value_FLOAT64,
				Value: &Value_DoubleValue{DoubleValue: x},
			}
		case string:
			val = &Value{
				Type:  Value_STRING,
				Value: &Value_StringValue{StringValue: x},
			}
		case time.Time:
			val = &Value{
				Type:  Value_TIMESTAMP,
				Value: &Value_TimestampValue{TimestampValue: timestamppb.New(x)},
			}
		case time.Duration:
			val = &Value{
				Type:  Value_DURATION,
				Value: &Value_DurationValue{DurationValue: durationpb.New(x)},
			}
		default:
			return nil, fmt.Errorf("unsupported type (%T) for field %s", v, k)
		}
		// Set the field value
		convFields[k] = val
	}
	// Return the Point
	return &Point{
		Name:      p.Name(),
		Fields:    convFields,
		Tags:      p.Tags(),
		Timestamp: timestamppb.New(p.Timestamp()),
	}, nil
}
