// Client describes the client-view of the metricsdb server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package metricsdb

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/metricsdb/internal/metricsdbrpc"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpcdialer"
	"bitbucket.org/pcastools/grpcutil/grpclog"
	"bitbucket.org/pcastools/grpcutil/grpcs2"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"io"
	"net/url"
	"strconv"
)

// Client is the client-view of the server.
type Client struct {
	log.BasicLogable
	io.Closer
	client metricsdbrpc.MetricsdbClient // The gRPC client
}

// Assert that a Client satisfies metrics.Interface.
var _ = metrics.Interface(&Client{})

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createDialFunc returns the dial function for the client. Assumes that the URI has scheme "tcp" or "ws", and that a port number is set.
func createDialFunc(uri string, lg log.Interface) grpc.DialOption {
	// Parse the URI
	u, err := url.ParseRequestURI(uri)
	if err != nil {
		panic("invalid URI: " + uri)
	}
	switch u.Scheme {
	case "tcp":
		// Connect via a TCP socket
		port, err := strconv.Atoi(u.Port())
		if err != nil {
			panic("invalid URI: " + uri)
		}
		return grpcdialer.TCPDialer(u.Hostname(), port, lg)
	case "ws":
		// Connect via a websocket
		return grpcdialer.WebSocketDialer(uri, lg)
	default:
		// Unknown scheme
		panic("unsupported URI scheme: " + u.Scheme)
	}
}

/////////////////////////////////////////////////////////////////////////
// Client functions
/////////////////////////////////////////////////////////////////////////

// NewClient returns a new client-view of the server specified by the configuration cfg.
func NewClient(ctx context.Context, cfg *ClientConfig) (*Client, error) {
	// Sanity check
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	// Create a new client
	c := &Client{}
	// Set the dial options
	dialOptions := []grpc.DialOption{
		createDialFunc(cfg.URI(), c.Log()),
		grpc.WithDefaultCallOptions(
			grpc.UseCompressor(grpcs2.Name),
		),
		grpc.WithChainUnaryInterceptor(
			grpclog.UnaryClientInterceptor(c.Log()),
		),
		grpc.WithChainStreamInterceptor(
			grpclog.StreamClientInterceptor(c.Log()),
		),
	}
	// Add the transport credentials
	var creds credentials.TransportCredentials
	if cfg.SSLDisabled {
		creds = insecure.NewCredentials()
	} else {
		creds = grpcutil.NewClientTLS(cfg.SSLCert)
	}
	dialOptions = append(dialOptions, grpc.WithTransportCredentials(creds))
	// Build a connection to the gRPC server
	conn, err := grpc.DialContext(ctx, cfg.Address.Hostname(), dialOptions...)
	if err != nil {
		return nil, err
	}
	// Save the connection on the client and return
	c.Closer = conn
	c.client = metricsdbrpc.NewMetricsdbClient(conn)
	return c, nil
}

// Submit submits the given point.
func (c *Client) Submit(ctx context.Context, p *metrics.Point) (err error) {
	// Sanity check
	if c == nil || c.client == nil {
		err = errors.New("uninitialised client")
		return
	} else if p == nil {
		return
	}
	// Convert the point
	var pt *metricsdbrpc.Point
	if pt, err = metricsdbrpc.ToPoint(p); err != nil {
		return
	}
	// Open the submit stream
	var stream metricsdbrpc.Metricsdb_SubmitClient
	if stream, err = c.client.Submit(ctx); err != nil {
		return
	}
	// Defer closing the stream
	defer func() {
		if _, closeErr := stream.CloseAndRecv(); err == nil {
			err = closeErr
		}
	}()
	// Submit the point
	err = stream.Send(pt)
	return
}

// SubmitSlice submits the given slice of points.
func (c *Client) SubmitSlice(ctx context.Context, ps []*metrics.Point) (err error) {
	// Sanity check
	if c == nil || c.client == nil {
		err = errors.New("uninitialised client")
		return
	} else if len(ps) == 0 {
		return
	}
	// Open the submit stream
	var stream metricsdbrpc.Metricsdb_SubmitClient
	if stream, err = c.client.Submit(ctx); err != nil {
		return
	}
	// Defer closing the stream
	defer func() {
		if _, closeErr := stream.CloseAndRecv(); err == nil {
			err = closeErr
		}
	}()
	// Start working through the points
	for _, p := range ps {
		if p != nil {
			// Convert the point
			var pt *metricsdbrpc.Point
			if pt, err = metricsdbrpc.ToPoint(p); err != nil {
				return
			}
			// Submit the point
			if err = stream.Send(pt); err != nil {
				return
			}
		}
	}
	return
}

// SubmitPoints submits the points in the given iterator.
func (c *Client) SubmitPoints(ctx context.Context, itr metrics.PointIterator) (err error) {
	// Sanity check
	if c == nil || c.client == nil {
		err = errors.New("uninitialised client")
		return
	}
	// Open the submit stream
	var stream metricsdbrpc.Metricsdb_SubmitClient
	if stream, err = c.client.Submit(ctx); err != nil {
		return
	}
	// Defer closing the stream
	defer func() {
		if _, closeErr := stream.CloseAndRecv(); err == nil {
			err = closeErr
		}
	}()
	// Start working through the points
	for itr.Next() {
		if p := itr.Value(); p != nil {
			// Convert the point
			var pt *metricsdbrpc.Point
			if pt, err = metricsdbrpc.ToPoint(p); err != nil {
				return
			}
			// Submit the point
			if err = stream.Send(pt); err != nil {
				return
			}
		}
	}
	// Note any error during iteration
	if err == nil {
		err = itr.Err()
	}
	return
}
