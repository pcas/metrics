// Package metricsdbflag provides a standard flag set for configuring the metrics package and starting metrics collection. Typical usage would be something like:
//
//	import(
//		"bitbucket.org/pcas/metrics/metricsdb/metricsdbflag"
//      "bitbucket.org/pcastools/flag"
//		"context"
//		...
//	)
//
// // parseArgs parses the command-line flags and environment variables.
//
// func parseArgs() error {
// 	// Define the usage message and command-line flags
// 	flag.SetGlobalHeader(fmt.Sprintf("...header for the usage message...")
// 	flag.SetName("Options")
// 	flag.Add(
// 		flag.String("foo", ...),
//      ...
// 	)
//  // Add the flagset that controls metrics collection
//	metricsSet := metricsdbflag.NewMetricsSet(nil)
// 	flag.AddSet(metricsSet)
// 	// Parse the flags
// 	flag.Parse()
//  // Do any additional sanity checks
//  ...
//	// Should we start collecting metrics?
//	if metricsSet.WithMetrics() {
//		if err := metricsdbflag.SetMetrics(
//			context.Background(),
//			metricsSet.ClientConfig(),
//			Name,
//		); err != nil {
//			return err
//		}
//	}
//  return nil
//  }
package metricsdbflag

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/bufmetrics"
	"bitbucket.org/pcas/metrics/metricsdb"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"time"
)

// Set represents a set of command-line flags defined by a client config.
type Set struct {
	c    *metricsdb.ClientConfig // The client config
	addr flag.Flag               // The address flag
}

// MetricsSet represents a set of command-line flags defined by a client config, and also provides a flag to turn metrics collection on or off.
type MetricsSet struct {
	*Set
	withMetricsFlag flag.Flag // The with-metrics flag
	withMetrics     bool      // Are metrics to be enabled?
}

/////////////////////////////////////////////////////////////////////////
// Set functions
/////////////////////////////////////////////////////////////////////////

// NewSet returns a set of command-line flags with defaults given by c. If c is nil then the metricsdb.DefaultConfig() will be used. Note that this does not update the default client config, nor does this update c. To recover the updated client config after parse, call the ClientConfig() method on the returned set.
func NewSet(c *metricsdb.ClientConfig) *Set {
	// Either move to the default client config, or move to a copy
	if c == nil {
		c = metricsdb.DefaultConfig()
	} else {
		c = c.Copy()
	}
	// Create the address flag
	addr := address.NewFlag(
		"metrics-address",
		&c.Address, c.Address,
		"The address of the pcas metrics server",
		address.EnvUsage("metrics-address", "PCAS_METRICS_ADDRESS"),
	)
	// Return the set
	return &Set{
		c:    c,
		addr: addr,
	}
}

// Flags returns the members of the set.
func (s *Set) Flags() []flag.Flag {
	if s == nil {
		return nil
	}
	return []flag.Flag{s.addr}
}

// Name returns the name of this collection of flags.
func (*Set) Name() string {
	return "Metrics options"
}

// UsageFooter returns the footer for the usage message for this flag set.
func (*Set) UsageFooter() string {
	return ""
}

// UsageHeader returns the header for the usage message for this flag set.
func (*Set) UsageHeader() string {
	return ""
}

// Validate validates the flag set.
func (s *Set) Validate() error {
	if s == nil {
		return errors.New("flag set is nil")
	}
	return s.c.Validate()
}

// ClientConfig returns the client config described by this set. This should only be called after the set has been successfully validated.
func (s *Set) ClientConfig() *metricsdb.ClientConfig {
	if s == nil {
		return metricsdb.DefaultConfig()
	}
	return s.c.Copy()
}

/////////////////////////////////////////////////////////////////////////
// MetricsSet functions
/////////////////////////////////////////////////////////////////////////

// NewMetricsSet returns a set of command-line flags as described by NewSet. In addition, this set provides a flag to turn metrics collection on or off.
func NewMetricsSet(c *metricsdb.ClientConfig) *MetricsSet {
	// Create the set
	s := &MetricsSet{
		Set: NewSet(c),
	}
	// Create the with-metrics flag
	s.withMetricsFlag = flag.Bool(
		"with-metrics",
		&s.withMetrics, s.withMetrics,
		"Collect metrics",
		"",
	)
	// Return the set
	return s
}

// Flags returns the members of the set.
func (s *MetricsSet) Flags() []flag.Flag {
	if s == nil {
		return nil
	}
	return append(s.Set.Flags(), s.withMetricsFlag)
}

// WithMetrics returns true iff the user selected to enable metrics collection. This should only be called after the set has been successfully validated.
func (s *MetricsSet) WithMetrics() bool {
	return s != nil && s.withMetrics
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// SetMetrics sets the global metrics metrics.Metrics() as specified by the client config c and tagged with the given program name. This will also update the default metrics config to c.
func SetMetrics(ctx context.Context, c *metricsdb.ClientConfig, name string) error {
	// Fetch or update the default client config
	if c == nil {
		c = metricsdb.DefaultConfig()
	} else {
		metricsdb.SetDefaultConfig(c)
	}
	// Create a new metricsdb client
	m, err := metricsdb.NewClient(ctx, c)
	if err != nil {
		return err
	}
	// Log to the default logger
	m.SetLogger(log.PrefixWith(log.Log(), "[metricsdb]"))
	// Wrap the client in a buffer and close it at cleanup
	b, err := bufmetrics.New(m, 1024, 5*time.Second, 10*time.Second)
	if err != nil {
		return err
	}
	cleanup.Add(b.Close)
	// Add the tags
	met := metrics.Interface(b)
	if len(name) != 0 {
		met = metrics.TagWith(met, metrics.Tags{
			"Program": name,
		})
	}
	// Set the global metrics endpoint
	metrics.SetMetrics(met)
	return nil
}
