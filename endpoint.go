// Endpoint describes the interface for a metrics endpoint.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package metrics

import (
	"bitbucket.org/pcastools/log"
	"context"
)

// PointIterator describe an iterator for Point data.
type PointIterator interface {
	Close() error  // Close closes the iterator, preventing further iteration.
	Err() error    // Err returns the last error, if any, encountered during iteration. Err may be called after Close.
	Next() bool    // Next advances the iterator. Returns true if there are more points in the iterator; false otherwise. Next must be called before the first call to Value.
	Value() *Point // Value returns the current point in the iterator. Attempting to call Value after the iterator is closed, or without a previous successful call to Next, may panic.
}

// Interface is a metrics endpoint that stores metric points.
type Interface interface {
	Submit(ctx context.Context, p *Point) error // Submit submits the given point.
}

// submitSlicer is the interface satisfied by the SubmitSlice method.
type submitSlicer interface {
	SubmitSlice(ctx context.Context, ps []*Point) error
}

// submitPointer is the interface satisfied by the SubmitPoints method.
type submitPointer interface {
	SubmitPoints(ctx context.Context, itr PointIterator) error
}

// Logger will log all metrics to a log.Interface.
type Logger struct {
	Log log.Interface // The destination log
}

// sliceIterator is a PointIterator that wraps a slice of *Points
type sliceIterator struct {
	pts      []*Point // The slice of points
	idx      int      // The current index in the slice
	isClosed bool     // Are we closed?
}

// discard discards all metrics.
type discard int

// Discard discards all metrics.
const Discard = discard(0)

/////////////////////////////////////////////////////////////////////////
// discard functions
/////////////////////////////////////////////////////////////////////////

// Submit is a no-op, and always returns nil.
func (discard) Submit(_ context.Context, _ *Point) error {
	return nil
}

// SubmitSlice is a no-op, and always returns nil.
func (discard) SubmitSlice(_ context.Context, _ []*Point) error {
	return nil
}

/////////////////////////////////////////////////////////////////////////
// sliceIterator functions
/////////////////////////////////////////////////////////////////////////

// IteratorFromSlice returns a PointIterator based on S.
func IteratorFromSlice(S []*Point) PointIterator {
	return &sliceIterator{
		pts: S,
		idx: -1,
	}
}

// Close closes the iterator.
func (s *sliceIterator) Close() error {
	s.isClosed = true
	return nil
}

// Err returns nil.
func (s *sliceIterator) Err() error {
	return nil
}

// Next advances the iterator.
func (s *sliceIterator) Next() bool {
	if s.idx == len(s.pts) {
		return false
	}
	s.idx++
	for s.idx < len(s.pts) {
		if s.pts[s.idx] != nil {
			return true
		}
		s.idx++
	}
	return false
}

// Value returns the current point in the iterator.
func (s *sliceIterator) Value() *Point {
	if s.isClosed {
		panic("Value requested from a closed iterator")
	} else if s.idx == -1 {
		panic("Value called before call to Next")
	} else if s.idx == len(s.pts) {
		panic("Value called after Next returned false")
	}
	return s.pts[s.idx]
}

/////////////////////////////////////////////////////////////////////////
// Logger functions
/////////////////////////////////////////////////////////////////////////

// Submit logs the given point.
func (l *Logger) Submit(_ context.Context, p *Point) error {
	if l != nil && l.Log != nil && p != nil {
		l.Log.Printf("%s", p)
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// SubmitSlice will submit the given slice of points to m. If m satisfies the interface
//	type SubmitSlicer interface {
//		SubmitSlice(context.Context, []*Point) error
//	}
// then m's SubmitSlice method will be used. Otherwise, if m satisfies the interface
//	type SubmitPointser interface {
//		SubmitPoints(context.Context, PointIterator) error
//	}
// then m's SubmitPoints method will be used.
func SubmitSlice(ctx context.Context, m Interface, pts []*Point) error {
	if n := len(pts); n == 0 {
		return nil
	} else if n == 1 {
		return m.Submit(ctx, pts[0])
	} else if s, ok := m.(submitSlicer); ok {
		return s.SubmitSlice(ctx, pts)
	} else if s, ok := m.(submitPointer); ok {
		return s.SubmitPoints(ctx, IteratorFromSlice(pts))
	}
	for _, p := range pts {
		if p != nil {
			if err := m.Submit(ctx, p); err != nil {
				return err
			}
		}
	}
	return nil
}

// SubmitPoints will submit the points in the given iterator to m. If m satisfies the interface
//	type SubmitPointser interface {
//		SubmitPoints(context.Context, PointIterator) error
//	}
// then m's SubmitPoints method will be used. Otherwise, if m satisfies the interface
//	type SubmitSlicer interface {
//		SubmitSlice(context.Context, []*Point) error
//	}
// then m's SubmitSlice method will be used.
func SubmitPoints(ctx context.Context, m Interface, itr PointIterator) error {
	// Does m have a SubmitPoints method?
	if s, ok := m.(submitPointer); ok {
		return s.SubmitPoints(ctx, itr)
	}
	// Does m have a SubmitSlice method?
	if s, ok := m.(submitSlicer); ok {
		// Create a local buffer
		const bufferSize = 512
		buf := make([]*Point, 0, bufferSize)
		// Start iterating over the points
		for itr.Next() {
			if p := itr.Value(); p != nil {
				buf = append(buf, p)
				if len(buf) == bufferSize {
					if err := s.SubmitSlice(ctx, buf); err != nil {
						return err
					}
					buf = make([]*Point, 0, bufferSize)
				}
			}
		}
		// Flush the buffer before returning
		if len(buf) != 0 {
			if err := s.SubmitSlice(ctx, buf); err != nil {
				return err
			}
		}
		return itr.Err()
	}
	// No luck -- we do this point-by-point
	for itr.Next() {
		if p := itr.Value(); p != nil {
			if err := m.Submit(ctx, p); err != nil {
				return err
			}
		}
	}
	return itr.Err()
}
