// Metrics describes the basic data types for a metric.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package metrics

import (
	"bitbucket.org/pcastools/gobutil"
	"encoding/gob"
	"errors"
	"fmt"
	"math"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

// valueType describes the type of a value in a Record.
type valueType uint8

// The type constants used when serialising a Record.
const (
	tInt = valueType(iota)
	tInt8
	tInt16
	tInt32
	tInt64
	tUint
	tUint8
	tUint16
	tUint32
	tUint64
	tBool
	tFloat64
	tString
	tTime
	tDuration
)

// globalTags is the map of global tags.
var globalTags = &sync.Map{}

// Fields is a map of field names and values. The values must take one of the following types:
//		time.Time
//		time.Duration
//		int, int8, int16, int32, int64
//		uint, uint8, uint16, uint32, uint64
//		float64
//		bool
//		string
type Fields map[string]interface{}

// Tags is a map of tag names and values.
type Tags map[string]string

// Point represents a point for a metric. That is, a unique tuple of name, fields, tags, and timestamp.
type Point struct {
	name      string    // The name
	fields    Fields    // The associated fields
	tags      Tags      // The associated tags
	timestamp time.Time // The timestamp
}

// SetMetricser is the interface satisfied by the SetMetrics method.
type SetMetricser interface {
	SetMetrics(m Interface) // SetMetrics sets a metrics endpoint.
}

// Metricser is the interface satisfied by the Metrics method.
type Metricser interface {
	Metrics() Interface // Metrics returns the metrics endpoint.
}

// Metricsable is the interface satisfied by the SetMetrics and Metrics methods. It indicates an object that supports metrics reporting.
type Metricsable interface {
	SetMetricser
	Metricser
}

//////////////////////////////////////////////////////////////////////
// valueType functions
//////////////////////////////////////////////////////////////////////

// typeForValue returns the valueType for the value x.
func typeForValue(x interface{}) (t valueType, ok bool) {
	ok = true
	switch x.(type) {
	case time.Time:
		t = tTime
	case time.Duration:
		t = tDuration
	case int:
		t = tInt
	case int8:
		t = tInt8
	case int16:
		t = tInt16
	case int32:
		t = tInt32
	case int64:
		t = tInt64
	case uint:
		t = tUint
	case uint8:
		t = tUint8
	case uint16:
		t = tUint16
	case uint32:
		t = tUint32
	case uint64:
		t = tUint64
	case bool:
		t = tBool
	case float64:
		t = tFloat64
	case string:
		t = tString
	default:
		ok = false
	}
	return
}

// Decode attempts to decode a value x of type t from the decoder dec.
func (t valueType) Decode(dec *gob.Decoder) (x interface{}, err error) {
	switch t {
	case tTime:
		var y time.Time
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tDuration:
		var y time.Duration
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tInt:
		var y int
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tInt8:
		var y int8
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tInt16:
		var y int16
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tInt32:
		var y int32
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tInt64:
		var y int64
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tUint:
		var y uint
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tUint8:
		var y uint8
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tUint16:
		var y uint16
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tUint32:
		var y uint32
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tUint64:
		var y uint64
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tBool:
		var y bool
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tFloat64:
		var y float64
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tString:
		var y string
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	default:
		err = errors.New("unknown type code")
	}
	if err != nil {
		err = fmt.Errorf("error decoding type %s: %w", t.String(), err)
	}
	return
}

// String returns a string description of the type.
func (t valueType) String() (s string) {
	switch t {
	case tTime:
		s = "time.Time"
	case tDuration:
		s = "time.Duration"
	case tInt:
		s = "int"
	case tInt8:
		s = "int8"
	case tInt16:
		s = "int16"
	case tInt32:
		s = "int32"
	case tInt64:
		s = "int64"
	case tUint:
		s = "uint"
	case tUint8:
		s = "uint8"
	case tUint16:
		s = "uint16"
	case tUint32:
		s = "uint32"
	case tUint64:
		s = "uint64"
	case tBool:
		s = "bool"
	case tFloat64:
		s = "float64"
	case tString:
		s = "string"
	default:
		s = strconv.Itoa(int(t))
	}
	return
}

/////////////////////////////////////////////////////////////////////////
// Fields functions
/////////////////////////////////////////////////////////////////////////

// IsValid returns true if and only if the fields are valid. If false, an error will be returned describing the problem. The fields are considered valid if and only if:
//	* the keys satisfy IsKeyValid;
//	* the values satisfy IsFieldValueValid.
func (f Fields) IsValid() (bool, error) {
	for k, v := range f {
		if ok, err := IsKeyValid(k); !ok {
			return false, fmt.Errorf("invalid key (%s): %w", k, err)
		} else if ok, err := IsFieldValueValid(v); !ok {
			return false, fmt.Errorf("invalid value for key %s: %w", k, err)
		}
	}
	return true, nil
}

// Keys returns a slice of keys of the fields. The keys are returned sorted using sort.Strings.
func (f Fields) Keys() []string {
	keys := make([]string, 0, len(f))
	for key := range f {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	return keys
}

// Copy returns a copy of the fields.
func (f Fields) Copy() Fields {
	fields := make(Fields, len(f))
	for k, v := range f {
		fields[k] = v
	}
	return fields
}

// GobEncode returns a byte slice representing the encoding of the fields.
func (f Fields) GobEncode() ([]byte, error) {
	// Build parallel slices of keys and types
	keys := make([]string, 0, len(f))
	types := make([]valueType, 0, len(f))
	for k, x := range f {
		keys = append(keys, k)
		t, ok := typeForValue(x)
		if !ok {
			return nil, fmt.Errorf("unsupported value type: %T", x)
		}
		types = append(types, t)
	}
	// Create an encoder
	enc := gobutil.NewEncoder()
	defer gobutil.ReuseEncoder(enc)
	// Encode the slice of keys and types
	if err := enc.Encode(keys); err != nil {
		return nil, err
	} else if err := enc.Encode(types); err != nil {
		return nil, err
	}
	// Encode the values one at a time
	for _, k := range keys {
		if err := enc.Encode(f[k]); err != nil {
			return nil, err
		}
	}
	// Return the byte slice
	return enc.Bytes(), nil
}

// GobDecode overwrites the receiver, which must be a pointer, with the value represented by the byte slice, which was written by GobEncode.
func (f *Fields) GobDecode(b []byte) error {
	// Create a decoder
	dec := gobutil.NewDecoder(b)
	// Decode the slice of keys and types
	var keys []string
	if err := dec.Decode(&keys); err != nil {
		return err
	}
	var types []valueType
	if err := dec.Decode(&types); err != nil {
		return err
	}
	// Sanity check
	size := len(keys)
	if len(types) != size {
		return fmt.Errorf("the number of keys (%d) does not equal the number of values (%d)", size, len(types))
	}
	// Allocate the memory
	*f = make(Fields, size)
	// Start decoding the values
	for i, t := range types {
		x, err := t.Decode(dec)
		if err != nil {
			return fmt.Errorf("unable to decode value for key %s: %w", keys[i], err)
		}
		(*f)[keys[i]] = x
	}
	return nil
}

// String returns a string representation of the fields.
func (f Fields) String() string {
	if len(f) == 0 {
		return "{}"
	}
	kvs := make([]string, 0, len(f))
	for k, v := range f {
		kvs = append(kvs, fmt.Sprintf("%s:%v", k, v))
	}
	return "{" + strings.Join(kvs, ",") + "}"
}

/////////////////////////////////////////////////////////////////////////
// Tags functions
/////////////////////////////////////////////////////////////////////////

// IsValid returns true if and only if the tags are valid. If false, an error will be returned describing the problem. The tags are considered valid if and only if:
//	* the keys satisfy IsKeyValid;
//	* the values satisfy IsTagValueValid.
func (t Tags) IsValid() (bool, error) {
	for k, v := range t {
		if ok, err := IsKeyValid(k); !ok {
			return false, fmt.Errorf("invalid key (%s): %w", k, err)
		} else if ok, err := IsTagValueValid(v); !ok {
			return false, fmt.Errorf("invalid value (%s) for key %s: %w", v, k, err)
		}
	}
	return true, nil
}

// Keys returns a slice of keys of the tags. The keys are returned sorted using sort.Strings.
func (t Tags) Keys() []string {
	keys := make([]string, 0, len(t))
	for key := range t {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	return keys
}

// Copy returns a copy of the tags.
func (t Tags) Copy() Tags {
	tags := make(Tags, len(t))
	for k, v := range t {
		tags[k] = v
	}
	return tags
}

// String returns a string representation of the tags.
func (t Tags) String() string {
	if len(t) == 0 {
		return "{}"
	}
	kvs := make([]string, 0, len(t))
	for k, v := range t {
		kvs = append(kvs, k+":"+v)
	}
	return "{" + strings.Join(kvs, ",") + "}"
}

/////////////////////////////////////////////////////////////////////////
// Global tags functions
/////////////////////////////////////////////////////////////////////////

// SetGlobalTag adds the tag with given key and value to the list of global tags. Any existing global tag with the same key will have its value replaced. Global tags are automatically added to the tags for any point  created with NewPoint or NewPointWithTimestamp. If a tag is provided to NewPoint or NewPointWithTimestamp with the same key as a global tag, then the point will use the provided value rather than the global value.
func SetGlobalTag(key string, value string) error {
	if ok, err := IsKeyValid(key); !ok {
		return fmt.Errorf("invalid key (%s): %w", key, err)
	} else if ok, err := IsTagValueValid(value); !ok {
		return fmt.Errorf("invalid value (%s) for key %s: %w", value, key, err)
	}
	globalTags.Store(key, value)
	return nil
}

// DeleteGlobalTag removes the tag with given key from the list of global tags.
func DeleteGlobalTag(key string) {
	globalTags.Delete(key)
}

// getGlobalTags returns the current global tags.
func getGlobalTags() Tags {
	tags := make(Tags)
	globalTags.Range(func(key interface{}, value interface{}) bool {
		tags[key.(string)] = value.(string)
		return true
	})
	return tags
}

/////////////////////////////////////////////////////////////////////////
// Point functions
/////////////////////////////////////////////////////////////////////////

// validatePointData validates the given point data. The data is considered valid if and only if:
//  * the name satisfies IsNameValid;
//  * the fields satisfy fields.IsValid;
//  * the tags satisfy tags.IsValid;
//  * the timestamp is non-zero.
// If validation fails, an error will be returned describing the problem.
func validatePointData(name string, fields Fields, tags Tags, timestamp time.Time) error {
	if ok, err := IsNameValid(name); !ok {
		return fmt.Errorf("invalid name (%s): %w", name, err)
	} else if ok, err := fields.IsValid(); !ok {
		return fmt.Errorf("invalid fields: %w", err)
	} else if ok, err := tags.IsValid(); !ok {
		return fmt.Errorf("invalid tags: %w", err)
	} else if timestamp.IsZero() {
		return errors.New("illegal zero timestamp")
	}
	return nil
}

// NewPoint returns a new point with the given data. Any global tags will also be included in the tags for this point. If a tag is provided with the same key as a global tag, then the point will use the provided value rather than the global value. The timestamp will be set to the current time. The data is considered valid if and only if:
//  * the name satisfies IsNameValid;
//  * the fields satisfy fields.IsValid;
//  * the tags satisfy tags.IsValid.
// If validation fails, an error will be returned describing the problem.
func NewPoint(name string, fields Fields, tags Tags) (*Point, error) {
	return NewPointWithTimestamp(name, fields, tags, time.Now())
}

// NewPointWithTimestamp returns a new point with the given data. Any global tags will also be included in the tags for this point. If a tag is provided with the same key as a global tag, then the point will use the provided value rather than the global value. The data is considered valid if and only if:
//  * the name satisfies IsNameValid;
//  * the fields satisfy fields.IsValid;
//  * the tags satisfy tags.IsValid;
//  * the timestamp is non-zero.
// If validation fails, an error will be returned describing the problem.
func NewPointWithTimestamp(name string, fields Fields, tags Tags, timestamp time.Time) (*Point, error) {
	// Sanity check
	if err := validatePointData(name, fields, tags, timestamp); err != nil {
		return nil, err
	}
	// Merge the global tags
	ptTags := getGlobalTags()
	for k, v := range tags {
		ptTags[k] = v
	}
	// Return the new point
	return &Point{
		name:      name,
		fields:    fields.Copy(),
		tags:      ptTags,
		timestamp: timestamp,
	}, nil
}

// RawPoint returns a new point with the given data. Unlike in the case of NewPoint and NewPointWithTimestamp, global tags will not be included in the returned point. The data is considered valid if and only if:
//  * the name satisfies IsNameValid;
//  * the fields satisfy fields.IsValid;
//  * the tags satisfy tags.IsValid;
//  * the timestamp is non-zero.
// If validation fails, an error will be returned describing the problem.
func RawPoint(name string, fields Fields, tags Tags, timestamp time.Time) (*Point, error) {
	// Sanity check
	if err := validatePointData(name, fields, tags, timestamp); err != nil {
		return nil, err
	}
	// Return the new point
	return &Point{
		name:      name,
		fields:    fields.Copy(),
		tags:      tags.Copy(),
		timestamp: timestamp,
	}, nil
}

// Name returns the name associated with this point.
func (p *Point) Name() string {
	if p == nil {
		return ""
	}
	return p.name
}

// Fields returns (a copy of) the fields associated with this point.
func (p *Point) Fields() Fields {
	if p == nil {
		return Fields{}
	}
	return p.fields.Copy()
}

// Tags returns (a copy of) the tags associated with this point.
func (p *Point) Tags() Tags {
	if p == nil {
		return Tags{}
	}
	return p.tags.Copy()
}

// Timestamp returns the timestamp associated with this point.
func (p *Point) Timestamp() time.Time {
	if p == nil {
		return time.Time{}
	}
	return p.timestamp
}

// GobEncode returns a byte slice representing the encoding of the point.
func (p *Point) GobEncode() ([]byte, error) {
	// Create an encoder
	enc := gobutil.NewEncoder()
	defer gobutil.ReuseEncoder(enc)
	// Encode the data
	if err := enc.Encode(p.name); err != nil {
		return nil, err
	} else if err = enc.Encode(p.fields); err != nil {
		return nil, err
	} else if err = enc.Encode(p.tags); err != nil {
		return nil, err
	} else if err = enc.Encode(p.timestamp); err != nil {
		return nil, err
	}
	// Return the byte slice
	return enc.Bytes(), nil
}

// GobDecode overwrites the receiver, which must be a valid pointer, with the value represented by the byte slice, which was written by GobEncode.
func (p *Point) GobDecode(b []byte) error {
	// Create a decoder
	dec := gobutil.NewDecoder(b)
	// Decode the data
	var name string
	if err := dec.Decode(&name); err != nil {
		return err
	}
	var fields Fields
	if err := dec.Decode(&fields); err != nil {
		return err
	}
	var tags Tags
	if err := dec.Decode(&tags); err != nil {
		return err
	}
	var timestamp time.Time
	if err := dec.Decode(&timestamp); err != nil {
		return err
	}
	// Sanity check
	if err := validatePointData(name, fields, tags, timestamp); err != nil {
		return err
	}
	// Set the data
	p.name = name
	p.fields = fields
	p.tags = tags
	p.timestamp = timestamp
	return nil
}

// String returns a string description of the point.
func (p *Point) String() string {
	if p == nil {
		return "<nil>"
	}
	return fmt.Sprintf("timestamp=%s name=%s fields=%s tags=%s", p.Timestamp(), p.Name(), p.fields, p.tags)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// IsNameValid validates the given name, returning true if and only if the name validates. A valid name must satisfy:
//	* non-empty
//	* length < 256
// with characters consisting only of the ASCII characters:
//	* a-z, A-Z, and 0-9
//	* '.', '-', and '_'.
// Any other character is considered invalid. In order to maintain compatibility with InfluxDB, the name "time" is prohibited. If validation fails, an error will be returned describing the problem.
func IsNameValid(name string) (bool, error) {
	if n := len(name); n == 0 {
		return false, errors.New("must not be empty")
	} else if n > math.MaxUint8 {
		return false, errors.New("length exceeds maximum")
	} else if name == "time" {
		return false, errors.New("the string \"time\" is prohibited")
	}
	var badr rune
	idx := strings.IndexFunc(name, func(r rune) bool {
		if !((r >= 'A' && r <= 'Z') || (r >= 'a' && r <= 'z') || (r >= '0' && r <= '9') || r == '.' || r == '-' || r == '_') {
			badr = r
			return true
		}
		return false
	})
	if idx != -1 {
		return false, fmt.Errorf("invalid character (%v)", badr)
	}
	return true, nil
}

// IsKeyValid validates the given key, returning true if and only if the key validates. A valid key must satisfy:
//  * non-empty
//  * length < 256
// with characters consisting only of the ASCII characters:
//	* a-z, A-Z, and 0-9
//	* '.', '-', and '_'.
// Any other character is considered invalid. In order to maintain compatibility with InfluxDB, the key "time" is prohibited. If validation fails, an error will be returned describing the problem.
func IsKeyValid(key string) (bool, error) {
	return IsNameValid(key)
}

// IsTagValueValid validates the given tag value, returning true if and only if the value validates. A valid tag value must satisfy:
//	* length < 256
// If validation fails, an error will be returned describing the problem.
func IsTagValueValid(value string) (bool, error) {
	if len(value) > math.MaxUint8 {
		return false, errors.New("length exceeds maximum")
	}
	return true, nil
}

// IsFieldValueValid validates the given field value, returning true if and only if value validates. A valid field value is one of the following types:
//	* time.Time
//	* time.Duration
//	* int, int8, int16, int32, int64
//	* uint, uint8, uint16, uint32, uint64
//	* float64
//	* bool
//	* string
// Any other type is considered invalid. In addition:
//  * if the value is a string, it must have length < 32768;
//  * if the value is a float64, it must not be NaN.
// If validation fails, an error will be returned describing the problem.
func IsFieldValueValid(value interface{}) (bool, error) {
	if _, ok := typeForValue(value); !ok {
		return false, fmt.Errorf("value is of an unsupported type (%T)", value)
	}
	switch value := value.(type) {
	case string:
		if len(value) > math.MaxInt16 {
			return false, errors.New("string length exceeds maximum")
		}
	case float64:
		if math.IsNaN(value) {
			return false, errors.New("NaN is an unsupported value for float64")
		}
	}
	return true, nil
}
