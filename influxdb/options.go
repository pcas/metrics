// Options describes the various options for the InfluxDB connection.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package influxdb

import (
	"time"
)

// Option sets options on an InfluxDB client connection.
type Option interface {
	apply(*clientOptions)
}

// funcOption wraps a function that modifies clientOptions into an implementation of the Option interface.
type funcOption struct {
	f func(*clientOptions)
}

// apply calls the wrapped function f on the given clientOptions.
func (h *funcOption) apply(do *clientOptions) {
	h.f(do)
}

// newFuncOption returns a funcOption wrapping f.
func newFuncOption(f func(*clientOptions)) *funcOption {
	return &funcOption{
		f: f,
	}
}

// clientOptions are the options on an influxdb.Client.
type clientOptions struct {
	Host      string        // The hostname of the server
	Port      int           // The port number of the server
	Username  string        // The username
	Password  string        // The password
	UserAgent string        // The user agent to use
	Timeout   time.Duration // The timeout when attempting to connect
}

// The default options values.
const (
	DefaultHost    = "localhost"
	DefaultPort    = 8086
	DefaultTimeout = time.Minute
)

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// parseOptions parses the given optional functions.
func parseOptions(options ...Option) *clientOptions {
	// Create the default options
	opts := &clientOptions{
		Host:    DefaultHost,
		Port:    DefaultPort,
		Timeout: DefaultTimeout,
	}
	// Set the options
	for _, h := range options {
		h.apply(opts)
	}
	return opts
}

// Host sets the hostname of the InfluxDB server.
func Host(host string) Option {
	return newFuncOption(func(opts *clientOptions) {
		opts.Host = host
	})
}

// Port sets the port of the InfluxDB server.
func Port(port int) Option {
	return newFuncOption(func(opts *clientOptions) {
		opts.Port = port
	})
}

// Username sets the username to use when connecting with the InfluxDB server.
func Username(username string) Option {
	return newFuncOption(func(opts *clientOptions) {
		opts.Username = username
	})
}

// Password sets the password to use when connecting with the InfluxDB server.
func Password(password string) Option {
	return newFuncOption(func(opts *clientOptions) {
		opts.Password = password
	})
}

// UserAgent sets the user agent to use when connecting with the InfluxDB server.
func UserAgent(useragent string) Option {
	return newFuncOption(func(opts *clientOptions) {
		opts.UserAgent = useragent
	})
}

// Timeout sets the timeout when attempting to communication with the InfluxDB server. A value of 0 means that there is no timeout.
func Timeout(d time.Duration) Option {
	return newFuncOption(func(opts *clientOptions) {
		opts.Timeout = d
	})
}
