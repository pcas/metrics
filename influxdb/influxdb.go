/*
Package influxdb provides an InfluxDB endpoint for metrics data.

InfluxDB is a time-series database:
   https://www.influxdata.com/time-series-platform/influxdb/

*/
package influxdb

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

import (
	"bitbucket.org/pcas/metrics"
	"context"
	"errors"
	"fmt"
	"github.com/influxdata/influxdb1-client/v2"
	"strconv"
	"sync"
)

// Client provides an InfluxDB client connection.
type Client struct {
	c        client.Client // The client connection to InfluxDB
	database string        // The database to connect to
	m        sync.RWMutex  // Controls access to the following
	isClosed bool          // Is the connection closed?
	closeErr error         // The error on close (if any)
}

// Assert that a Client satisfies metrics.Interface.
var _ = metrics.Interface(&Client{})

/////////////////////////////////////////////////////////////////////////
// Client functions
/////////////////////////////////////////////////////////////////////////

// New returns a connection to InfluxDB.
func New(database string, optional ...Option) (*Client, error) {
	// Sanity check
	if len(database) == 0 {
		return nil, errors.New("the InfluxDB database name must not be empty")
	}
	// Parse the options
	opts := parseOptions(optional...)
	if opts.Port <= 0 || opts.Port > 65535 {
		return nil, fmt.Errorf("the port number (%d) must be in the range 1-65535", opts.Port)
	}
	// Create the connection
	c, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:      "http://" + opts.Host + ":" + strconv.Itoa(opts.Port),
		Username:  opts.Username,
		Password:  opts.Password,
		UserAgent: opts.UserAgent,
		Timeout:   opts.Timeout,
	})
	if err != nil {
		return nil, fmt.Errorf("unable to create new client connection to InfluxDB: %w", err)
	}
	// Wrap this up and return
	return &Client{
		c:        c,
		database: database,
	}, nil
}

// Close prevent further points being submitted.
func (c *Client) Close() error {
	c.m.Lock()
	defer c.m.Unlock()
	if !c.isClosed {
		c.isClosed = true
		c.closeErr = c.c.Close()
	}
	return c.closeErr
}

// Submit submits the given point.
func (c *Client) Submit(_ context.Context, p *metrics.Point) error {
	// Is there anything to do?
	if p == nil {
		return nil
	}
	// Create a new batch points
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database: c.database,
	})
	if err != nil {
		return fmt.Errorf("error creating batch points: %w", err)
	}
	// Add the point
	pt, err := client.NewPoint(p.Name(), p.Tags(), p.Fields(), p.Timestamp())
	if err != nil {
		return fmt.Errorf("error converting point format: %w", err)
	}
	bp.AddPoint(pt)
	// Acquire a read lock and check that we're not closed
	c.m.RLock()
	defer c.m.RUnlock()
	if c.isClosed {
		return errors.New("attempting to submit metric after Close has been called")
	}
	// Submit the batch points
	return c.c.Write(bp)
}

// SubmitSlice submits the slice of points.
func (c *Client) SubmitSlice(_ context.Context, ps []*metrics.Point) error {
	// Is there anything to do?
	if len(ps) == 0 {
		return nil
	}
	// Create a new batch points
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database: c.database,
	})
	if err != nil {
		return fmt.Errorf("error creating batch points: %w", err)
	}
	// Add the points
	for _, p := range ps {
		if p != nil {
			pt, err := client.NewPoint(p.Name(), p.Tags(), p.Fields(), p.Timestamp())
			if err != nil {
				return fmt.Errorf("error converting point format: %w", err)
			}
			bp.AddPoint(pt)
		}
	}
	// Acquire a read lock and check that we're not closed
	c.m.RLock()
	defer c.m.RUnlock()
	if c.isClosed {
		return errors.New("attempting to submit metric after Close has been called")
	}
	// Submit the batch points
	return c.c.Write(bp)
}

// SubmitPoints submits the points in the given iterator.
func (c *Client) SubmitPoints(_ context.Context, itr metrics.PointIterator) error {
	// Create a new batch points
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database: c.database,
	})
	if err != nil {
		return fmt.Errorf("error creating batch points: %w", err)
	}
	// Add the points
	for itr.Next() {
		if p := itr.Value(); p != nil {
			pt, err := client.NewPoint(p.Name(), p.Tags(), p.Fields(), p.Timestamp())
			if err != nil {
				return fmt.Errorf("error converting point format: %w", err)
			}
			bp.AddPoint(pt)
		}
	}
	// Acquire a read lock and check that we're not closed
	c.m.RLock()
	defer c.m.RUnlock()
	if c.isClosed {
		return errors.New("attempting to submit metric after Close has been called")
	}
	// Submit the batch points
	if err = c.c.Write(bp); err == nil {
		err = itr.Err()
	}
	return err
}
