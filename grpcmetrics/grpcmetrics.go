// grpcmetrics provides interceptors for gRPC that record metric data.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package grpcmetrics

import (
	"bitbucket.org/pcas/metrics"
	"context"
	"google.golang.org/grpc"
	"strings"
	"time"
)

// timeout is the maximum amount of time allowed attempting to submit a metric point.
const timeout = 100 * time.Millisecond

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// submitPoint submits a point to the metrics endpoint m. Any errors when submitting the point will be silently ignored.
func submitPoint(m metrics.Interface, method string, d time.Duration) {
	name := strings.Trim(strings.ReplaceAll(method, "/", "."), ".")
	pt, err := metrics.NewPoint(name, metrics.Fields{
		"Time": d,
	}, nil)
	if err == nil {
		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()
		m.Submit(ctx, pt) // Ignore any error
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// UnaryServerInterceptor returns the unary server interceptor submitting metrics to m.
func UnaryServerInterceptor(m metrics.Interface) grpc.UnaryServerInterceptor {
	if m == nil {
		panic("Illegal nil argument")
	}
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		t := time.Now()
		resp, err := handler(ctx, req)
		submitPoint(m, info.FullMethod, time.Since(t))
		return resp, err
	}
}

// StreamServerInterceptor returns the stream server interceptor submitting metrics to m.
func StreamServerInterceptor(m metrics.Interface) grpc.StreamServerInterceptor {
	if m == nil {
		panic("Illegal nil argument")
	}
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		t := time.Now()
		err := handler(srv, stream)
		submitPoint(m, info.FullMethod, time.Since(t))
		return err
	}
}

// UnaryClientInterceptor returns the unary client interceptor submitting metrics to m.
func UnaryClientInterceptor(m metrics.Interface) grpc.UnaryClientInterceptor {
	if m == nil {
		panic("Illegal nil argument")
	}
	return func(ctx context.Context, method string, req interface{}, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		t := time.Now()
		err := invoker(ctx, method, req, reply, cc, opts...)
		submitPoint(m, method, time.Since(t))
		return err
	}
}

// StreamClientInterceptor returns the stream client interceptor submitting metrics to m.
func StreamClientInterceptor(m metrics.Interface) grpc.StreamClientInterceptor {
	if m == nil {
		panic("Illegal nil argument")
	}
	return func(ctx context.Context, desc *grpc.StreamDesc, cc *grpc.ClientConn, method string, streamer grpc.Streamer, opts ...grpc.CallOption) (grpc.ClientStream, error) {
		t := time.Now()
		stream, err := streamer(ctx, desc, cc, method, opts...)
		submitPoint(m, method, time.Since(t))
		return stream, err
	}
}
