// Global provides a global metrics endpoint

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package metrics

// met contains the current global metrics endpoint.
var met = &SwapMetrics{}

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// SetMetrics changes the global metrics endpoint to m.
func SetMetrics(m Interface) {
	if m == nil {
		m = Discard
	}
	met.Swap(m)
}

// Metrics returns the current global metrics endpoint.
func Metrics() Interface {
	return met
}
