module bitbucket.org/pcas/metrics

go 1.16

require (
	bitbucket.org/pcas/logger v0.1.43
	bitbucket.org/pcas/sslflag v0.0.16
	bitbucket.org/pcastools/address v0.1.4
	bitbucket.org/pcastools/cleanup v1.0.4
	bitbucket.org/pcastools/convert v1.0.5
	bitbucket.org/pcastools/flag v0.0.19
	bitbucket.org/pcastools/gobutil v1.0.4
	bitbucket.org/pcastools/grpcutil v1.0.14
	bitbucket.org/pcastools/hash v1.0.5
	bitbucket.org/pcastools/listenutil v0.0.10
	bitbucket.org/pcastools/log v1.0.4
	bitbucket.org/pcastools/version v0.0.5
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/influxdata/influxdb1-client v0.0.0-20220302092344-a9ab5670611c
	github.com/rcrowley/go-metrics v0.0.0-20201227073835-cf1acfcdf475
	github.com/shirou/gopsutil v3.21.11+incompatible
	github.com/tklauser/go-sysconf v0.3.10 // indirect
	github.com/tklauser/numcpus v0.5.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	google.golang.org/grpc v1.54.0
	google.golang.org/protobuf v1.30.0
)
