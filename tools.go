// Tools provides some useful Interface implementations.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package metrics

import (
	"context"
	"fmt"
	"io"
	"sync"
	"sync/atomic"
)

// tagWithIterator wraps an iterator, adding tags to the points.
type tagWithIterator struct {
	tags Tags          // The tags to add
	itr  PointIterator // The underlying iterator
}

// tagWith adds tags to the wrapped Interface.
type tagWith struct {
	tags Tags      // The tags to add
	m    Interface // The underlying Interface
}

// SwapMetrics wraps an Interface that may be safely replaced while other go routines use the SwapMetrics concurrently. The zero value for a SwapMetrics will discard all metrics without error.
type SwapMetrics struct {
	m atomic.Value
}

// BasicMetricsable provides an embeddable implementation of a Metricsable.
type BasicMetricsable struct {
	m SwapMetrics
}

// swapMetricsStruct is used by SwapMetrics to encapsulate an Interface.
type swapMetricsStruct struct {
	Interface
}

// ToMany combines multiple Interfaces, sending all metrics to each of the Interfaces. ToMany is safe to be used concurrently. The zero value for a ToMany will discard all metrics without an error.
type ToMany struct {
	mutex sync.RWMutex // Mutex protecting the slice of Interfaces
	ms    []Interface  // The slice of Interfaces
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// isSubmittingToDiscard returns true if the ultimate Interface is recognised as nil or Discard.
func isSubmittingToDiscard(m Interface) bool {
	if m == nil || m == Discard {
		return true
	} else if s, ok := m.(*tagWith); ok {
		return isSubmittingToDiscard(s.m)
	} else if s, ok := m.(*SwapMetrics); ok {
		return isSubmittingToDiscard(unwrap(s.Unwrap()))
	} else if s, ok := m.(*ToMany); ok {
		return s.isDiscard()
	}
	return false
}

// mergeTagsIntoPoint returns a new point based on p, given by merging the given tags. In the case of duplicate tag keys, the values from p will be used.
func mergeTagsIntoPoint(p *Point, tags Tags) *Point {
	// Create the new tags
	newTags := make(Tags, len(p.tags)+len(tags))
	for k, v := range tags {
		newTags[k] = v
	}
	for k, v := range p.tags {
		newTags[k] = v
	}
	// Create a new point with the new tags
	return &Point{
		name:      p.name,
		fields:    p.fields,
		tags:      newTags,
		timestamp: p.timestamp,
	}
}

/////////////////////////////////////////////////////////////////////////
// tagWithIterator functions
/////////////////////////////////////////////////////////////////////////

// Close closes the iterator, preventing further iteration.
func (itr *tagWithIterator) Close() error {
	return itr.itr.Close()
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (itr *tagWithIterator) Err() error {
	return itr.itr.Err()
}

// Next advances the iterator. Returns true if there are more points in the iterator; false otherwise. Next must be called before the first call to Value.
func (itr *tagWithIterator) Next() bool {
	return itr.itr.Next()
}

// Value returns the current point in the iterator. Attempting to call Value after the iterator is closed, or without a previous successful call to Next, may panic.
func (itr *tagWithIterator) Value() *Point {
	return mergeTagsIntoPoint(itr.itr.Value(), itr.tags)
}

/////////////////////////////////////////////////////////////////////////
// tagWith functions
/////////////////////////////////////////////////////////////////////////

// TagWith wraps the given Interface so that all metrics will have the given tags included. Any duplicate tag keys on Interface with will be replaced with the new tag values. If the given tags fail to validate, this will panic.
func TagWith(m Interface, tags Tags) Interface {
	// Is there anything to do?
	if m == nil || m == Discard {
		return Discard
	} else if len(tags) == 0 {
		return m
	} else if ok, err := tags.IsValid(); !ok {
		panic(fmt.Sprintf("Invalid tags: %s", err))
	}
	// If m is a tagWith instance, we copy over the data
	if s, ok := m.(*tagWith); ok {
		t := make(Tags, len(tags)+len(s.tags))
		for k, v := range s.tags {
			t[k] = v
		}
		for k, v := range tags {
			t[k] = v
		}
		return &tagWith{
			tags: t,
			m:    s.m,
		}
	}
	// Wrap the Interface in a tagWith instance
	return &tagWith{
		tags: tags.Copy(),
		m:    m,
	}
}

// Close calls Close on the underlying Interface, if the underlying Interface satisfies the io.Closer interface. Otherwise it does nothing.
func (s *tagWith) Close() error {
	if c, ok := s.m.(io.Closer); ok {
		return c.Close()
	}
	return nil
}

// Submit submits the given point to the wrapped Interface.
func (s *tagWith) Submit(ctx context.Context, p *Point) error {
	if p == nil {
		return nil
	}
	m := unwrap(s.m)
	if isSubmittingToDiscard(m) {
		return nil
	}
	return m.Submit(ctx, mergeTagsIntoPoint(p, s.tags))
}

// SubmitSlice submits the given slice of points to the wrapped Interface.
func (s *tagWith) SubmitSlice(ctx context.Context, ps []*Point) error {
	// Is there anything to do?
	if len(ps) == 0 {
		return nil
	}
	m := unwrap(s.m)
	if isSubmittingToDiscard(m) {
		return nil
	}
	// Create a new slice
	qs := make([]*Point, 0, len(ps))
	for _, p := range ps {
		if p != nil {
			qs = append(qs, mergeTagsIntoPoint(p, s.tags))
		}
	}
	// Submit this new slice to the underlying database
	return SubmitSlice(ctx, m, qs)
}

// SubmitPoints submits the points in the given iterator to the wrapped Interface.
func (s *tagWith) SubmitPoints(ctx context.Context, itr PointIterator) error {
	return SubmitPoints(ctx, unwrap(s.m), &tagWithIterator{
		tags: s.tags,
		itr:  itr,
	})
}

/////////////////////////////////////////////////////////////////////////
// SwapMetrics functions
/////////////////////////////////////////////////////////////////////////

// unwrap checks whether the given Interface is a SwapMetrics and, if so, recursively calls unwrap until the first non-SwapMetrics is reached.
func unwrap(m Interface) Interface {
	if m == nil || m == Discard {
		return Discard
	} else if s, ok := m.(*SwapMetrics); ok {
		return unwrap(s.Unwrap())
	}
	return m
}

// Close calls Close on the wrapped Interface, if the wrapped Interface satisfies the io.Closer interface. Otherwise it does nothing.
func (s *SwapMetrics) Close() error {
	m := unwrap(s.Unwrap())
	if c, ok := m.(io.Closer); ok {
		return c.Close()
	}
	return nil
}

// Unwrap returns the wrapped Interface. If the wrapped Interface is nil, then Discard is returned.
func (s *SwapMetrics) Unwrap() Interface {
	if s == nil {
		return Discard
	} else if l, ok := s.m.Load().(swapMetricsStruct); ok {
		return l.Interface
	}
	return Discard
}

// Swap replaces the currently wrapped Interface with m. This will panic if s is nil.
func (s *SwapMetrics) Swap(m Interface) {
	s.m.Store(swapMetricsStruct{m})
}

// Submit submits the given point to the wrapped Interface.
func (s *SwapMetrics) Submit(ctx context.Context, p *Point) error {
	m := unwrap(s.Unwrap())
	if isSubmittingToDiscard(m) {
		return nil
	}
	return m.Submit(ctx, p)
}

// SubmitSlice submits the given slice of points to the wrapped Interface.
func (s *SwapMetrics) SubmitSlice(ctx context.Context, ps []*Point) error {
	m := unwrap(s.Unwrap())
	if isSubmittingToDiscard(m) {
		return nil
	}
	return SubmitSlice(ctx, m, ps)
}

// SubmitPoints submits the points in the given iterator to the wrapped Interface.
func (s *SwapMetrics) SubmitPoints(ctx context.Context, itr PointIterator) error {
	return SubmitPoints(ctx, unwrap(s.Unwrap()), itr)
}

/////////////////////////////////////////////////////////////////////////
// BasicMetricsable functions
/////////////////////////////////////////////////////////////////////////

// SetMetrics sets a metrics endpoint.
func (b *BasicMetricsable) SetMetrics(m Interface) {
	if b != nil {
		b.m.Swap(m)
	}
}

// Metrics returns the metrics endpoint.
func (b *BasicMetricsable) Metrics() Interface {
	if b == nil {
		return Discard
	}
	return &b.m
}

/////////////////////////////////////////////////////////////////////////
// ToMany functions
/////////////////////////////////////////////////////////////////////////

// isDiscard returns true if it can be determined that all metrics will be discarded.
func (s *ToMany) isDiscard() bool {
	if s != nil {
		s.mutex.RLock()
		for _, m := range s.ms {
			if !isSubmittingToDiscard(m) {
				s.mutex.RUnlock()
				return false
			}
		}
		s.mutex.RUnlock()
	}
	return true
}

// Len returns the number of Interfaces being used.
func (s *ToMany) Len() (n int) {
	if s != nil {
		s.mutex.RLock()
		n = len(s.ms)
		s.mutex.RUnlock()
	}
	return
}

// Metrics returns the slice of Interfaces being used.
func (s *ToMany) Metrics() []Interface {
	if s == nil {
		return []Interface{}
	}
	s.mutex.RLock()
	ms := make([]Interface, len(s.ms))
	copy(ms, s.ms)
	s.mutex.RUnlock()
	return ms
}

// Reset returns the current slice of Interfaces being used, and empties this slice.
func (s *ToMany) Reset() []Interface {
	if s == nil {
		return []Interface{}
	}
	s.mutex.Lock()
	ms := s.ms
	s.ms = nil
	s.mutex.Unlock()
	if ms == nil {
		ms = []Interface{}
	}
	return ms
}

// Append appends the given Interfaces to the slice of Interfaces being used. This will panic if s is nil.
func (s *ToMany) Append(m ...Interface) {
	if len(m) != 0 {
		s.mutex.Lock()
		if s.ms == nil {
			s.ms = make([]Interface, 0, len(m))
		}
		s.ms = append(s.ms, m...)
		s.mutex.Unlock()
	}
}

// Close calls Close on each of the associated Interfaces, if the Interface satisfies the io.Closer interface.
func (s *ToMany) Close() (err error) {
	if s != nil {
		s.mutex.RLock()
		for _, m := range s.ms {
			if c, ok := m.(io.Closer); ok {
				if closeErr := c.Close(); err == nil {
					err = closeErr
				}
			}
		}
		s.mutex.RUnlock()
	}
	return
}

// Submit submits the given point to the Interfaces.
func (s *ToMany) Submit(ctx context.Context, p *Point) (err error) {
	if s != nil {
		s.mutex.RLock()
		for _, m := range s.ms {
			if !isSubmittingToDiscard(m) {
				if e := m.Submit(ctx, p); err == nil {
					err = e
				}
			}
		}
		s.mutex.RUnlock()
	}
	return
}

// SubmitSlice submits the given slice of points to the Interfaces.
func (s *ToMany) SubmitSlice(ctx context.Context, ps []*Point) (err error) {
	if s != nil {
		s.mutex.RLock()
		n := len(s.ms) - 1
		for i, m := range s.ms {
			if !isSubmittingToDiscard(m) {
				var psCopy []*Point
				if i == n {
					psCopy = ps
				} else {
					psCopy = make([]*Point, len(ps))
					copy(psCopy, ps)
				}
				if e := SubmitSlice(ctx, m, psCopy); err == nil {
					err = e
				}
			}
		}
		s.mutex.RUnlock()
	}
	return
}
