// Runtimemetrics sets up collection of metrics for CPU usage, memory usage, GC performance, etc.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package runtimemetrics

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/convert"
	"context"
	"github.com/shirou/gopsutil/process"
	"os"
	stdmetrics "runtime/metrics"
	"time"
)

// reportEvery is the time interval at which we report metrics
const reportEvery = 5 * time.Second

// reportTimeout is the timeout when reporting metrics
const reportTimeout = 3 * time.Second

// Stopper represents something that has a Stop method
type Stopper interface {
	Stop() error
}

// reporter contains a channel which should be closed to stop metrics collection
type reporter struct {
	cancel context.CancelFunc
}

// metricsSample is a map of metrics we will collect, to their user-facing name
var metricsSample = map[string]string{
	"/gc/cycles/total:gc-cycles":   "GCCycles",
	"/gc/heap/allocs:bytes":        "AllocsBytes",
	"/gc/heap/allocs:objects":      "AllocsObjects",
	"/gc/heap/frees:bytes":         "FreesBytes",
	"/gc/heap/frees:objects":       "FreesObjects",
	"/sched/goroutines:goroutines": "GoRoutines",
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// reportMetrics sends metrics on CPU usage etc. to the metrics endpoint m.
func reportMetrics(ctx context.Context, m metrics.Interface, samples []stdmetrics.Sample, p *process.Process) {
	// Create a slice of metric points
	pts := make([]*metrics.Point, 0, 2)
	// We report a single CPU metric, with several fields
	if p != nil {
		// Collect the data
		cpu := make(metrics.Fields, 4)
		// Report CPU times
		if t, err := p.Times(); err == nil {
			cpu["Time"] = t.Total()
			cpu["UserTime"] = t.User
			cpu["SystemTime"] = t.System
		}
		// Report CPU percentage
		if x, err := p.CPUPercent(); err == nil {
			cpu["Percent"] = x
		}
		// If there's anything to do, create the point
		if len(cpu) != 0 {
			if pt, err := metrics.NewPoint("CPU", cpu, nil); err == nil {
				pts = append(pts, pt)
			}
		}
	}
	// We report a single memory metric, with several fields.
	stdmetrics.Read(samples)
	mem := make(metrics.Fields, len(samples))
	for _, sample := range samples {
		// Recover the name
		if name, ok := metricsSample[sample.Name]; ok {
			// Note the value and switch on the kind
			value := sample.Value
			switch value.Kind() {
			case stdmetrics.KindUint64:
				mem[name] = value.Uint64()
			case stdmetrics.KindFloat64:
				mem[name] = value.Float64()
			default:
				// We silently ignore any other kinds
			}
		}
	}
	// Create the point
	if pt, err := metrics.NewPoint("Memory", mem, nil); err == nil {
		pts = append(pts, pt)
	}
	// Is there anything to do?
	if len(pts) == 0 {
		return
	}
	// Submit the points
	ctx, cancel := context.WithTimeout(ctx, reportTimeout)
	defer cancel()
	metrics.SubmitSlice(ctx, m, pts) // Ignore any error
}

/////////////////////////////////////////////////////////////////////////
// reporter functions
/////////////////////////////////////////////////////////////////////////

// Stop closes the done channel, thereby stopping metrics collection
func (r *reporter) Stop() error {
	r.cancel()
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Start launches background goroutines that write metrics for CPU usage etc. to m. The caller should call Stop on the returned Stopper to shut down metrics collection.
func Start(m metrics.Interface) Stopper {
	// Grab our process
	p, err := process.NewProcess(int32(os.Getpid()))
	if err != nil {
		p = nil
	}
	// We tag metrics with the hostname and PID, if possible
	tags := make(metrics.Tags, 2)
	x, _ := convert.ToString(os.Getpid())
	tags["PID"] = x
	if h, err := os.Hostname(); err == nil {
		tags["Hostname"] = h
	}
	m = metrics.TagWith(m, tags)
	// Create a sample for the metrics we're interested in.
	samples := make([]stdmetrics.Sample, 0, len(metricsSample))
	for name := range metricsSample {
		var sample stdmetrics.Sample
		sample.Name = name
		samples = append(samples, sample)
	}
	// Create a context for the background worker
	ctx, cancel := context.WithCancel(context.Background())
	// Start the background worker
	go func(ctx context.Context) {
		t := time.NewTicker(reportEvery)
		defer t.Stop()
		for {
			select {
			case <-t.C:
				reportMetrics(ctx, m, samples, p)
			case <-ctx.Done():
				return
			}
		}
	}(ctx)
	return &reporter{cancel: cancel}
}
