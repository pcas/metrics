// Example is an example program making use of the metrics package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/metricsreporter"
	"bitbucket.org/pcas/metrics/runtimemetrics"
	"bitbucket.org/pcastools/log"
	gometrics "github.com/rcrowley/go-metrics"
	"time"
)

func generateMetrics() {
	c := gometrics.NewCounter()
	if err := gometrics.Register("foo", c); err != nil {
		panic(err)
	}
	g := gometrics.NewGauge()
	if err := gometrics.Register("bar", g); err != nil {
		panic(err)
	}
	for now := range time.Tick(time.Second) {
		c.Inc(1)
		g.Update(now.Unix())
	}
}

func main() {
	// Send metrics to stderr
	m := &metrics.Logger{Log: log.Stderr}
	// Collect runtime metrics
	defer runtimemetrics.Start(m).Stop() // Ignore any error
	// Link go-metrics' default register to m
	defer metricsreporter.Register(
		m,
		gometrics.DefaultRegistry,
		5*time.Second,
		log.PrefixWith(log.Stderr, "[metricsreporter]"),
	).Stop()
	// Generate some metrics for 30 seconds, then exit
	go generateMetrics()
	<-time.After(30 * time.Second)
}
