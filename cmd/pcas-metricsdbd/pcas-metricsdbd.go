// Pcas-metricsdbd is a server that logs metrics data.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/bufmetrics"
	"bitbucket.org/pcas/metrics/influxdb"
	"bitbucket.org/pcas/metrics/metricsdb"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/listenutil"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"strconv"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// connectToInfluxDB returns a client connection to InfluxDB.
func connectToInfluxDB(opts *Options, lg log.Interface) (*influxdb.Client, error) {
	// Provide some logging feedback about the connection
	lg.Printf(`Connecting to InfluxDB via http://%s/
influxdb-timeout=%s
influxdb-username=%s`,
		opts.InfluxDBAddress,
		opts.InfluxDBTimeout,
		opts.InfluxDBUsername,
	)
	// Build the options
	options := []influxdb.Option{
		influxdb.Host(opts.InfluxDBAddress.Hostname()),
		influxdb.Username(opts.InfluxDBUsername),
		influxdb.Password(opts.InfluxDBPassword),
		influxdb.Timeout(opts.InfluxDBTimeout),
	}
	if opts.InfluxDBAddress.HasPort() {
		options = append(options, influxdb.Port(opts.InfluxDBAddress.Port()))
	}
	// Dial a connection to InfluxDB
	return influxdb.New(opts.InfluxDBDatabase, options...)
}

// assignTags wraps the given metrics.Interface to use the given tags.
func assignTags(m metrics.Interface, tags metrics.Tags, lg log.Interface) metrics.Interface {
	lg.Printf("Global tags settings:\n  tags=%s", tags)
	if len(tags) != 0 {
		m = metrics.TagWith(m, tags)
	}
	return m
}

// connectToEndpoint returns the metrics.Interface to which all metrics data is sent.
func connectToEndpoint(opts *Options, lg log.Interface) (metrics.Interface, error) {
	// Establish a connection to InfluxDB
	m, err := connectToInfluxDB(opts, lg)
	if err != nil {
		return nil, err
	}
	// Assign the global tags
	assignTags(m, opts.Tags, lg)
	// Provide some logging feedback about the buffer settings
	lg.Printf(`Metric data buffered with settings:
  flush-interval=%s
  flush-timeout=%s
  max-buffer-size=%d`,
		opts.FlushInterval,
		opts.FlushTimeout,
		opts.MaxBufferSize,
	)
	// Create the buffer
	b, err := bufmetrics.New(m, opts.MaxBufferSize, opts.FlushInterval, opts.FlushTimeout)
	if err != nil {
		return nil, err
	}
	// Set the logger and return
	b.SetLogger(log.PrefixWith(lg, "[buffer]"))
	return b, nil
}

/// createTCPListener returns a new TCP listener.
func createTCPListener(a *address.Address, opts *Options, lg log.Interface) (net.Listener, error) {
	port := metricsdb.DefaultTCPPort
	if a.HasPort() {
		port = a.Port()
	}
	return listenutil.TCPListener(a.Hostname(), port,
		listenutil.MaxNumConnections(opts.MaxNumConnections),
		listenutil.Log(lg),
	)
}

// createWebsocketListener returns a new websocket listener.
func createWebsocketListener(a *address.Address, opts *Options, lg log.Interface) (net.Listener, error) {
	port := metricsdb.DefaultWSPort
	if a.HasPort() {
		port = a.Port()
	}
	uri := "ws://" + a.Hostname() + ":" + strconv.Itoa(port) + a.EscapedPath()
	l, shutdown, err := listenutil.WebsocketListenAndServe(uri,
		listenutil.MaxNumConnections(opts.MaxNumConnections),
		listenutil.Log(lg),
	)
	if err != nil {
		return nil, err
	}
	cleanup.Add(shutdown)
	return l, nil
}

// createListener returns a new listener.
func createListener(opts *Options, lg log.Interface) (l net.Listener, err error) {
	switch opts.Address.Scheme() {
	case "tcp":
		l, err = createTCPListener(opts.Address, opts, lg)
	case "ws":
		l, err = createWebsocketListener(opts.Address, opts, lg)
	default:
		err = errors.New("unsupported URI scheme: " + opts.Address.Scheme())
	}
	if err != nil {
		lg.Printf("Error starting listener: %v", err)
	}
	return
}

// run starts up and runs the server. Use the given context to shutdown.
func run(ctx context.Context, opts *Options, m metrics.Interface, lg log.Interface) error {
	// Build the options
	serverOpts := []metricsdb.Option{
		metricsdb.Metrics(m),
	}
	if len(opts.SSLKey) == 0 {
		lg.Printf("SSL is disabled: connections to this server are not encrypted")
	} else {
		serverOpts = append(serverOpts, metricsdb.SSLCertAndKey(opts.SSLKeyCert, opts.SSLKey))
	}
	// Create the server
	s, err := metricsdb.NewServer(serverOpts...)
	if err != nil {
		return err
	}
	s.SetLogger(log.PrefixWith(lg, "[metricsdbserver]"))
	// If the context fires, stop the server
	go func() {
		<-ctx.Done()
		s.GracefulStop()
	}()
	// Create the listener and serve any incoming connections
	l, err := createListener(opts, lg)
	if err != nil {
		return err
	}
	defer l.Close()
	return s.Serve(l)
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Create the connection to the metrics endpoint
	var m metrics.Interface
	if m, err = connectToEndpoint(opts, lg); err != nil {
		return
	}
	// Defer closing the endpoint
	defer func() {
		if cl, ok := m.(io.Closer); ok {
			if closeErr := cl.Close(); err == nil {
				err = closeErr
			}
		}
	}()
	// Recover and log any server panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("panic in main: %v", e)
			}
		}
	}()
	// Run the server
	err = run(ctx, opts, m, lg)
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
