// Config.go handles configuration and logging for the pcas metrics server

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/influxdb"
	"bitbucket.org/pcas/metrics/metricsdb"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"context"
	"errors"
	"fmt"
	"math"
	"os"
	"strings"
	"time"
)

// Options describes the configuration options.
type Options struct {
	// Monitordbd server options
	Address           *address.Address // The address to bind to
	SSLKey            []byte           // The SSL private key
	SSLKeyCert        []byte           // The SSL private key certificate
	FlushInterval     time.Duration    // The time between flushes
	FlushTimeout      time.Duration    // Timeout after which flush is abandoned
	MaxBufferSize     int              // The maximum metrics buffer size
	MaxNumConnections int              // The maximum number of connections
	Tags              metrics.Tags     // The global tags to associate with metrics
	// InfluxDB options
	InfluxDBAddress  *address.Address // The address of the server
	InfluxDBUsername string           // The username
	InfluxDBPassword string           // The password
	InfluxDBTimeout  time.Duration    // The timeout when attempting to connect
	InfluxDBDatabase string           // The database to write metric data
}

// Name is the name of the executable.
const Name = "pcas-metricsdbd"

// The default values.
const (
	DefaultHostname          = "localhost"
	DefaultPort              = metricsdb.DefaultTCPPort
	DefaultFlushInterval     = 5 * time.Second
	DefaultFlushTimeout      = 10 * time.Minute
	DefaultMaxBufferSize     = 2048
	DefaultMaxNumConnections = 1024
	DefaultInfluxDBHostname  = influxdb.DefaultHost
	DefaultInfluxDBPort      = influxdb.DefaultPort
	DefaultInfluxDBTimeout   = influxdb.DefaultTimeout
	DefaultInfluxDBDatabase  = "metrics"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nill. If the error is non-nill then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Create the default address
	addr, err := address.NewTCP(DefaultHostname, DefaultPort)
	if err != nil {
		panic(err) // This should never happen
	}
	// Create the default InfluxDB address
	influxDBAddr, err := address.NewTCP(DefaultInfluxDBHostname, DefaultInfluxDBPort)
	if err != nil {
		panic(err) // This should never happen
	}
	// Return the default options
	return &Options{
		Address:           addr,
		FlushInterval:     DefaultFlushInterval,
		FlushTimeout:      DefaultFlushTimeout,
		MaxBufferSize:     DefaultMaxBufferSize,
		MaxNumConnections: DefaultMaxNumConnections,
		InfluxDBAddress:   influxDBAddr,
		InfluxDBTimeout:   DefaultInfluxDBTimeout,
		InfluxDBDatabase:  DefaultInfluxDBDatabase,
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if opts.MaxBufferSize <= 0 {
		return fmt.Errorf("invalid maximum buffer size (%d)", opts.MaxBufferSize)
	} else if opts.MaxNumConnections <= 0 || opts.MaxNumConnections > math.MaxInt32 {
		return fmt.Errorf("invalid maximum number of connections (%d)", opts.MaxNumConnections)
	} else if opts.FlushInterval <= 0 {
		return fmt.Errorf("the flush interval (%s) must be positive", opts.FlushInterval)
	} else if opts.FlushTimeout <= 0 {
		return fmt.Errorf("the flush timeout (%s) must be positive", opts.FlushTimeout)
	} else if !opts.InfluxDBAddress.IsTCP() {
		return fmt.Errorf("the InfluxDB address (%s) must be in the form \"hostname[:port]\"", opts.InfluxDBAddress)
	} else if opts.InfluxDBTimeout <= 0 {
		return fmt.Errorf("the InfluxDB timeout (%s) must be positive", opts.InfluxDBTimeout)
	} else if len(opts.InfluxDBDatabase) == 0 {
		return errors.New("the InfluxDB database name must not be empty")
	}
	return nil
}

// parseTags attempts to parse the string in the format "key1:val1,key2:val2,..." into a map.
func parseTags(str string) (metrics.Tags, error) {
	// Is there anything to do?
	str = strings.TrimSpace(str)
	if len(str) == 0 {
		return nil, nil
	}
	// Split the string on commas
	S := strings.Split(str, ",")
	// Try and convert each entry into a key:value pair
	tags := make(metrics.Tags, len(S))
	for i, s := range S {
		T := strings.SplitN(s, ":", 2)
		if len(T) != 2 {
			return nil, fmt.Errorf("unable to parse tag %d (%s) into key:value pair", i+1, s)
		}
		tags[strings.TrimSpace(T[0])] = strings.TrimSpace(T[1])
	}
	// Do the tags validate?
	if ok, err := tags.IsValid(); !ok {
		return nil, fmt.Errorf("invalid tags: %w", err)
	}
	return tags, nil
}

// setLoggers starts the loggers as indicated by the flags.
func setLoggers(sslClientSet *sslflag.ClientSet, logdSet *logdflag.LogSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Note the loggers we'll use
	toStderr := logdSet.ToStderr()
	toServer := logdSet.ToServer()
	// Create the config
	c := logdSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the loggers
	return logdflag.SetLogger(ctx, toStderr, toServer, c, Name)
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	var tags string
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s is the pcas metrics server.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		address.NewFlag("address", &opts.Address, opts.Address, "The address to bind to", "The value of the flag -address should either take the form \"hostname[:port]\" or be a web-socket URI \"ws://host/path\"."),
		flag.Duration("flush-interval", &opts.FlushInterval, opts.FlushInterval, "The time between flushes", ""),
		flag.Duration("flush-timeout", &opts.FlushTimeout, opts.FlushTimeout, "The timeout after which a flush will be abandoned", ""),
		flag.String("influxdb-database", &opts.InfluxDBDatabase, opts.InfluxDBDatabase, "The InfluxDB database to use", ""),
		address.NewFlag("influxdb-address", &opts.InfluxDBAddress, opts.InfluxDBAddress, "The InfluxDB server address", ""),
		flag.String("influxdb-password", &opts.InfluxDBPassword, opts.InfluxDBPassword, "The password to use when connecting to the InfluxDB server", ""),
		flag.Duration("influxdb-timeout", &opts.InfluxDBTimeout, opts.InfluxDBTimeout, "The timeout for connection attempts to the InfluxDB server", ""),
		flag.String("influxdb-username", &opts.InfluxDBUsername, opts.InfluxDBUsername, "The username to use when connecting to the InfluxDB server", ""),
		flag.Int("max-buffer-size", &opts.MaxBufferSize, opts.MaxBufferSize, "The maximum metrics buffer size", ""),
		flag.Int("max-num-connections", &opts.MaxNumConnections, opts.MaxNumConnections, "The maximum number of connections", ""),
		flag.String("tags", &tags, tags, "The global tags, in the form \"key1:val1,key2:val2,...\"", ""),
		&version.Flag{AppName: Name},
	)
	// Create and add the logd flag set
	logdSet := logdflag.NewLogSet(nil)
	flag.AddSet(logdSet)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Create and add the standard SSL server set
	sslServerSet := &sslflag.ServerSet{}
	flag.AddSet(sslServerSet)
	// Parse the flags
	flag.Parse()
	// Recover the SSL server details
	opts.SSLKey = sslServerSet.Key()
	opts.SSLKeyCert = sslServerSet.Certificate()
	// Parse the tags
	var err error
	if opts.Tags, err = parseTags(tags); err != nil {
		return err
	}
	// Validate the options
	if err = validate(opts); err != nil {
		return err
	}
	// Set the loggers
	return setLoggers(sslClientSet, logdSet)
}
